package com.mobile.trato.io.tratoapp

import android.app.Activity
import com.identy.IdentySdk
import com.identy.WSQCompression
import com.identy.enums.FingerDetectionMode
import org.json.JSONObject
import java.lang.Exception

object FingerIdManager {

    var callback: Callback? = null

    private val detectionModes: Array<FingerDetectionMode> =
            arrayOf(FingerDetectionMode.L4F,
                    FingerDetectionMode.R4F,
                    FingerDetectionMode.LEFT_THUMB,
                    FingerDetectionMode.RIGHT_THUMB)
    private val compression = WSQCompression.WSQ_10_1

    private val enrollListener = object : IdentySdk.IdentyResponseListener {
        override fun onResponse(response: IdentySdk.IdentyResponse?) {
            callback?.onSuccess(response?.toJson().toString())
        }

        override fun onErrorResponse(error: IdentySdk.IdentyError?) {
            callback?.onError(errorSDK = error?.message.toString())
        }
    }


    fun startFingerIdProtocol(activity: Activity, callback: Callback) {
        IdentySdk.newInstance(
                activity, "app/src/main/assets",
                { d ->
                    try {
                        d?.setDisplayImages(true)?.setMode("commercial")
                                ?.setAS(false)?.setDetectionMode(detectionModes)
                                ?.displayImages(true)
                                ?.setWSQCompression(compression)
                                ?.capture()
                    } catch (e: Exception) {
                        callback.onError(e.localizedMessage.toString())
                    }
                }, enrollListener, "AIzaSyAkJgWng1csvFjOgqJheGu7VCeObX6EGXQ", true
        )
    }

    interface Callback {
        fun onSuccess(response: String)
        fun onError(errorSDK: String)
    }

}