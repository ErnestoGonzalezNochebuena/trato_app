package com.mobile.trato.io.tratoapp;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.identy.IdentySdk;
import com.identy.WSQCompression;
import com.identy.enums.FingerDetectionMode;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;


public class MainActivity extends FlutterActivity {

    private static final String appChannelName = "trato.io";
    private static final String fingerIdPlugin = "fingerid.plugin";
    private static final String getFingerId = "getFingerId";

    private FingerDetectionMode[] detectionModes;
    WSQCompression compression = WSQCompression.WSQ_10_1;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), (appChannelName + fingerIdPlugin))
                .setMethodCallHandler((call, result) -> {
                    if (call.method.equals(getFingerId)) {
                        MainActivity.this.getFingerprints(new FingerIdManager.Callback() {
                            @Override
                            public void onSuccess(@NotNull String response) {
                                result.success(response);
                            }

                            @Override
                            public void onError(@NotNull String errorSDK) {
                                result.error(errorSDK, errorSDK, errorSDK);
                            }
                        });
                    } else {
                        result.notImplemented();
                    }
                });

    }

    public void getFingerprints(FingerIdManager.Callback callback) {
        detectionModes = new FingerDetectionMode[]{FingerDetectionMode.L4F, FingerDetectionMode.R4F, FingerDetectionMode.LEFT_THUMB, FingerDetectionMode.RIGHT_THUMB};

        IdentySdk.IdentyResponseListener enrollListener = new IdentySdk.IdentyResponseListener() {
            @Override
            public void onResponse(IdentySdk.IdentyResponse response) {
                JSONObject jo = response.toJson();
                callback.onSuccess(jo.toString());
            }

            @Override
            public void onErrorResponse(IdentySdk.IdentyError error) {
                callback.onError(error.getMessage());
            }
        };

        try {
            IdentySdk.newInstance(MainActivity.this, "com.mobile.trato.io.tratoapp2020-09-17 00_00_00.lic", d -> {
                try {
                    d.setDisplayImages(true).setMode("commercial")
                            .setAS(false).
                            setDetectionMode(detectionModes).
                            setWSQCompression(compression).
                            capture();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, enrollListener, "AIzaSyAkJgWng1csvFjOgqJheGu7VCeObX6EGXQ", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        System.gc();
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.gc();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
