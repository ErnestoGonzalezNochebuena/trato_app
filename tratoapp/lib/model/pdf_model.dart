import 'package:tratoapp/model/base_model.dart';

class PdfModel extends Model {
  String url;

  PdfModel();

  factory PdfModel.fromJson(Map<String, dynamic> json) {
    PdfModel pdfModel = PdfModel();
    pdfModel.url = json["url"];
    return pdfModel;
  }
}
