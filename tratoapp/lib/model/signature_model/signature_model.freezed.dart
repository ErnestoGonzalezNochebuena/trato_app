// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'signature_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
SignatureModel _$SignatureModelFromJson(Map<String, dynamic> json) {
  return _SignatureModel.fromJson(json);
}

class _$SignatureModelTearOff {
  const _$SignatureModelTearOff();

  _SignatureModel call(
      {List<List<CoordenateModel>> coords,
      dynamic date,
      double width,
      double height,
      bool success,
      String error,
      bool signAsObject}) {
    return _SignatureModel(
      coords: coords,
      date: date,
      width: width,
      height: height,
      success: success,
      error: error,
      signAsObject: signAsObject,
    );
  }
}

// ignore: unused_element
const $SignatureModel = _$SignatureModelTearOff();

mixin _$SignatureModel {
  List<List<CoordenateModel>> get coords;
  dynamic get date;
  double get width;
  double get height;
  bool get success;
  String get error;
  bool get signAsObject;

  Map<String, dynamic> toJson();
  $SignatureModelCopyWith<SignatureModel> get copyWith;
}

abstract class $SignatureModelCopyWith<$Res> {
  factory $SignatureModelCopyWith(
          SignatureModel value, $Res Function(SignatureModel) then) =
      _$SignatureModelCopyWithImpl<$Res>;
  $Res call(
      {List<List<CoordenateModel>> coords,
      dynamic date,
      double width,
      double height,
      bool success,
      String error,
      bool signAsObject});
}

class _$SignatureModelCopyWithImpl<$Res>
    implements $SignatureModelCopyWith<$Res> {
  _$SignatureModelCopyWithImpl(this._value, this._then);

  final SignatureModel _value;
  // ignore: unused_field
  final $Res Function(SignatureModel) _then;

  @override
  $Res call({
    Object coords = freezed,
    Object date = freezed,
    Object width = freezed,
    Object height = freezed,
    Object success = freezed,
    Object error = freezed,
    Object signAsObject = freezed,
  }) {
    return _then(_value.copyWith(
      coords: coords == freezed
          ? _value.coords
          : coords as List<List<CoordenateModel>>,
      date: date == freezed ? _value.date : date as dynamic,
      width: width == freezed ? _value.width : width as double,
      height: height == freezed ? _value.height : height as double,
      success: success == freezed ? _value.success : success as bool,
      error: error == freezed ? _value.error : error as String,
      signAsObject:
          signAsObject == freezed ? _value.signAsObject : signAsObject as bool,
    ));
  }
}

abstract class _$SignatureModelCopyWith<$Res>
    implements $SignatureModelCopyWith<$Res> {
  factory _$SignatureModelCopyWith(
          _SignatureModel value, $Res Function(_SignatureModel) then) =
      __$SignatureModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<List<CoordenateModel>> coords,
      dynamic date,
      double width,
      double height,
      bool success,
      String error,
      bool signAsObject});
}

class __$SignatureModelCopyWithImpl<$Res>
    extends _$SignatureModelCopyWithImpl<$Res>
    implements _$SignatureModelCopyWith<$Res> {
  __$SignatureModelCopyWithImpl(
      _SignatureModel _value, $Res Function(_SignatureModel) _then)
      : super(_value, (v) => _then(v as _SignatureModel));

  @override
  _SignatureModel get _value => super._value as _SignatureModel;

  @override
  $Res call({
    Object coords = freezed,
    Object date = freezed,
    Object width = freezed,
    Object height = freezed,
    Object success = freezed,
    Object error = freezed,
    Object signAsObject = freezed,
  }) {
    return _then(_SignatureModel(
      coords: coords == freezed
          ? _value.coords
          : coords as List<List<CoordenateModel>>,
      date: date == freezed ? _value.date : date as dynamic,
      width: width == freezed ? _value.width : width as double,
      height: height == freezed ? _value.height : height as double,
      success: success == freezed ? _value.success : success as bool,
      error: error == freezed ? _value.error : error as String,
      signAsObject:
          signAsObject == freezed ? _value.signAsObject : signAsObject as bool,
    ));
  }
}

@JsonSerializable()
class _$_SignatureModel extends _SignatureModel with DiagnosticableTreeMixin {
  const _$_SignatureModel(
      {this.coords,
      this.date,
      this.width,
      this.height,
      this.success,
      this.error,
      this.signAsObject})
      : super._();

  factory _$_SignatureModel.fromJson(Map<String, dynamic> json) =>
      _$_$_SignatureModelFromJson(json);

  @override
  final List<List<CoordenateModel>> coords;
  @override
  final dynamic date;
  @override
  final double width;
  @override
  final double height;
  @override
  final bool success;
  @override
  final String error;
  @override
  final bool signAsObject;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SignatureModel(coords: $coords, date: $date, width: $width, height: $height, success: $success, error: $error, signAsObject: $signAsObject)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SignatureModel'))
      ..add(DiagnosticsProperty('coords', coords))
      ..add(DiagnosticsProperty('date', date))
      ..add(DiagnosticsProperty('width', width))
      ..add(DiagnosticsProperty('height', height))
      ..add(DiagnosticsProperty('success', success))
      ..add(DiagnosticsProperty('error', error))
      ..add(DiagnosticsProperty('signAsObject', signAsObject));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignatureModel &&
            (identical(other.coords, coords) ||
                const DeepCollectionEquality().equals(other.coords, coords)) &&
            (identical(other.date, date) ||
                const DeepCollectionEquality().equals(other.date, date)) &&
            (identical(other.width, width) ||
                const DeepCollectionEquality().equals(other.width, width)) &&
            (identical(other.height, height) ||
                const DeepCollectionEquality().equals(other.height, height)) &&
            (identical(other.success, success) ||
                const DeepCollectionEquality()
                    .equals(other.success, success)) &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)) &&
            (identical(other.signAsObject, signAsObject) ||
                const DeepCollectionEquality()
                    .equals(other.signAsObject, signAsObject)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(coords) ^
      const DeepCollectionEquality().hash(date) ^
      const DeepCollectionEquality().hash(width) ^
      const DeepCollectionEquality().hash(height) ^
      const DeepCollectionEquality().hash(success) ^
      const DeepCollectionEquality().hash(error) ^
      const DeepCollectionEquality().hash(signAsObject);

  @override
  _$SignatureModelCopyWith<_SignatureModel> get copyWith =>
      __$SignatureModelCopyWithImpl<_SignatureModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SignatureModelToJson(this);
  }
}

abstract class _SignatureModel extends SignatureModel {
  const _SignatureModel._() : super._();
  const factory _SignatureModel(
      {List<List<CoordenateModel>> coords,
      dynamic date,
      double width,
      double height,
      bool success,
      String error,
      bool signAsObject}) = _$_SignatureModel;

  factory _SignatureModel.fromJson(Map<String, dynamic> json) =
      _$_SignatureModel.fromJson;

  @override
  List<List<CoordenateModel>> get coords;
  @override
  dynamic get date;
  @override
  double get width;
  @override
  double get height;
  @override
  bool get success;
  @override
  String get error;
  @override
  bool get signAsObject;
  @override
  _$SignatureModelCopyWith<_SignatureModel> get copyWith;
}
