import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:tratoapp/model/coord_model/coord_model.dart';

part 'signature_model.freezed.dart';
part 'signature_model.g.dart';

@freezed
abstract class SignatureModel implements _$SignatureModel {
  const SignatureModel._();

  const factory SignatureModel(
      {List<List<CoordenateModel>> coords,
      dynamic date,
      double width,
      double height,
      bool success,
      String error,
      bool signAsObject}) = _SignatureModel;


  factory SignatureModel.fromJson(Map<String, dynamic> json) =>
      _$SignatureModelFromJson(json);

}

//  SignatureModel();
//
//  factory SignatureModel.fromJson(Map<String, dynamic> json) =>
//      _$SignatureModelFromJson(json);
//
//  static Map<String, dynamic> toJson(SignatureModel model) {
//    Map<String, dynamic> signatureCoordsMap = new HashMap();
//    Map<String, dynamic> map = new HashMap();
//    map['coords'] = model.coords
//        .map((list) =>
//        list
//            .map((e) =>
//            CoordenateModel.toJson(CoordenateModel(
//                x: roundDouble(e.offset.dx, 1),
//                y: roundDouble(e.offset.dy, 1),
//                time: DateTime
//                    .now()
//                    .millisecondsSinceEpoch,
//                color: "black")))
//            .toList())
//        .toList();
//    map['date'] = model.date;
//    map['width'] = model.width;
//    map['height'] = model.height;
//    signatureCoordsMap['signatureCoords'] = map;
//    // var jsonRequest = json.encode(signatureCoordsMap);
////    String result = '{' +
////        'coords:${model.coords},' +
////        'date:${model.date},' +
////        'width:${model.width},' +
////        'height:${model.height}' +
////        '}';
//////    jsonRequest = jsonRequest.replaceAll("[\\-\\+\\^:,]", "");
////    result = result.replaceAll("\\", '');
//    return signatureCoordsMap;
//    // return jsonRequest;
//  }
//
//  factory SignatureModel.fromJson(Map<String, dynamic> json) {
//    SignatureModel model = SignatureModel();
//    model.success = json["success"];
//    model.error = json["error"];
//    return model;
//  }
//
//  static String generateCoords(List<Point> points) {
//    List<List<Point>> request = List();
//    request.add(points);
//    String jsonRequest = json.encode(request
//        .map((list) =>
//        list
//            .map((e) =>
//            CoordenateModel.toJson(CoordenateModel(
//                x: roundDouble(e.offset.dx, 1),
//                y: roundDouble(e.offset.dy, 1),
//                time: DateTime
//                    .now()
//                    .millisecondsSinceEpoch,
//                color: 'black')))
//            .toList())
//        .toList());
//    String result = "[[";
//    points.forEach((element) {
//      result = result +
//          '{"x":${roundDouble(element.offset.dx, 1)},' +
//          '"y":${roundDouble(element.offset.dy, 1)},' +
//          '"time":${DateTime
//              .now()
//              .millisecondsSinceEpoch},' +
//          '"color":"black"},';
//    });
//    result = result + "]]";
//    result = jsonRequest.replaceAll("\\", '');
//    // result = result.replaceRange(start, end, replacement)
//    // return result;
//    return jsonRequest;
//  }
//
//  static double roundDouble(double value, int places) {
//    double mod = pow(10.0, places);
//    return ((value * mod).round().toDouble() / mod);
//  }
//}
