// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signature_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SignatureModel _$_$_SignatureModelFromJson(Map<String, dynamic> json) {
  return _$_SignatureModel(
    coords: (json['coords'] as List)
        ?.map((e) => (e as List)
            ?.map((e) => e == null
                ? null
                : CoordenateModel.fromJson(e as Map<String, dynamic>))
            ?.toList())
        ?.toList(),
    date: json['date'],
    width: (json['width'] as num)?.toDouble(),
    height: (json['height'] as num)?.toDouble(),
    success: json['success'] as bool,
    error: json['error'] as String,
    signAsObject: json['signAsObject'] as bool,
  );
}

Map<String, dynamic> _$_$_SignatureModelToJson(_$_SignatureModel instance) =>
    <String, dynamic>{
      'coords': instance.coords,
      'date': instance.date,
      'width': instance.width,
      'height': instance.height,
      'success': instance.success,
      'error': instance.error,
      'signAsObject': instance.signAsObject,
    };
