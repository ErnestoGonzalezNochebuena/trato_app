// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coord_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CoordenateModel _$_$_CoordenateModelFromJson(Map<String, dynamic> json) {
  return _$_CoordenateModel(
    x: (json['x'] as num)?.toDouble(),
    y: (json['y'] as num)?.toDouble(),
    time: json['time'],
    color: json['color'] as String,
  );
}

Map<String, dynamic> _$_$_CoordenateModelToJson(_$_CoordenateModel instance) =>
    <String, dynamic>{
      'x': instance.x,
      'y': instance.y,
      'time': instance.time,
      'color': instance.color,
    };
