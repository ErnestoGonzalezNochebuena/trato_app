import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'coord_model.freezed.dart';
part 'coord_model.g.dart';

@freezed
abstract class CoordenateModel implements _$CoordenateModel {
  const CoordenateModel._();

  const factory CoordenateModel(
      {double x, double y, dynamic time, String color}) = _CoordenateModel;

  factory CoordenateModel.fromJson(Map<String, dynamic> json) =>
      _$CoordenateModelFromJson(json);
}
