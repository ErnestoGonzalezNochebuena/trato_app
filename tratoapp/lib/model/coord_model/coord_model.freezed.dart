// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'coord_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CoordenateModel _$CoordenateModelFromJson(Map<String, dynamic> json) {
  return _CoordenateModel.fromJson(json);
}

class _$CoordenateModelTearOff {
  const _$CoordenateModelTearOff();

  _CoordenateModel call({double x, double y, dynamic time, String color}) {
    return _CoordenateModel(
      x: x,
      y: y,
      time: time,
      color: color,
    );
  }
}

// ignore: unused_element
const $CoordenateModel = _$CoordenateModelTearOff();

mixin _$CoordenateModel {
  double get x;
  double get y;
  dynamic get time;
  String get color;

  Map<String, dynamic> toJson();
  $CoordenateModelCopyWith<CoordenateModel> get copyWith;
}

abstract class $CoordenateModelCopyWith<$Res> {
  factory $CoordenateModelCopyWith(
          CoordenateModel value, $Res Function(CoordenateModel) then) =
      _$CoordenateModelCopyWithImpl<$Res>;
  $Res call({double x, double y, dynamic time, String color});
}

class _$CoordenateModelCopyWithImpl<$Res>
    implements $CoordenateModelCopyWith<$Res> {
  _$CoordenateModelCopyWithImpl(this._value, this._then);

  final CoordenateModel _value;
  // ignore: unused_field
  final $Res Function(CoordenateModel) _then;

  @override
  $Res call({
    Object x = freezed,
    Object y = freezed,
    Object time = freezed,
    Object color = freezed,
  }) {
    return _then(_value.copyWith(
      x: x == freezed ? _value.x : x as double,
      y: y == freezed ? _value.y : y as double,
      time: time == freezed ? _value.time : time as dynamic,
      color: color == freezed ? _value.color : color as String,
    ));
  }
}

abstract class _$CoordenateModelCopyWith<$Res>
    implements $CoordenateModelCopyWith<$Res> {
  factory _$CoordenateModelCopyWith(
          _CoordenateModel value, $Res Function(_CoordenateModel) then) =
      __$CoordenateModelCopyWithImpl<$Res>;
  @override
  $Res call({double x, double y, dynamic time, String color});
}

class __$CoordenateModelCopyWithImpl<$Res>
    extends _$CoordenateModelCopyWithImpl<$Res>
    implements _$CoordenateModelCopyWith<$Res> {
  __$CoordenateModelCopyWithImpl(
      _CoordenateModel _value, $Res Function(_CoordenateModel) _then)
      : super(_value, (v) => _then(v as _CoordenateModel));

  @override
  _CoordenateModel get _value => super._value as _CoordenateModel;

  @override
  $Res call({
    Object x = freezed,
    Object y = freezed,
    Object time = freezed,
    Object color = freezed,
  }) {
    return _then(_CoordenateModel(
      x: x == freezed ? _value.x : x as double,
      y: y == freezed ? _value.y : y as double,
      time: time == freezed ? _value.time : time as dynamic,
      color: color == freezed ? _value.color : color as String,
    ));
  }
}

@JsonSerializable()
class _$_CoordenateModel extends _CoordenateModel with DiagnosticableTreeMixin {
  const _$_CoordenateModel({this.x, this.y, this.time, this.color}) : super._();

  factory _$_CoordenateModel.fromJson(Map<String, dynamic> json) =>
      _$_$_CoordenateModelFromJson(json);

  @override
  final double x;
  @override
  final double y;
  @override
  final dynamic time;
  @override
  final String color;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CoordenateModel(x: $x, y: $y, time: $time, color: $color)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'CoordenateModel'))
      ..add(DiagnosticsProperty('x', x))
      ..add(DiagnosticsProperty('y', y))
      ..add(DiagnosticsProperty('time', time))
      ..add(DiagnosticsProperty('color', color));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CoordenateModel &&
            (identical(other.x, x) ||
                const DeepCollectionEquality().equals(other.x, x)) &&
            (identical(other.y, y) ||
                const DeepCollectionEquality().equals(other.y, y)) &&
            (identical(other.time, time) ||
                const DeepCollectionEquality().equals(other.time, time)) &&
            (identical(other.color, color) ||
                const DeepCollectionEquality().equals(other.color, color)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(x) ^
      const DeepCollectionEquality().hash(y) ^
      const DeepCollectionEquality().hash(time) ^
      const DeepCollectionEquality().hash(color);

  @override
  _$CoordenateModelCopyWith<_CoordenateModel> get copyWith =>
      __$CoordenateModelCopyWithImpl<_CoordenateModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CoordenateModelToJson(this);
  }
}

abstract class _CoordenateModel extends CoordenateModel {
  const _CoordenateModel._() : super._();
  const factory _CoordenateModel(
      {double x, double y, dynamic time, String color}) = _$_CoordenateModel;

  factory _CoordenateModel.fromJson(Map<String, dynamic> json) =
      _$_CoordenateModel.fromJson;

  @override
  double get x;
  @override
  double get y;
  @override
  dynamic get time;
  @override
  String get color;
  @override
  _$CoordenateModelCopyWith<_CoordenateModel> get copyWith;
}
