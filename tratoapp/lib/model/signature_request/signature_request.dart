import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:tratoapp/model/coord_model/coord_model.dart';
import 'package:tratoapp/model/location_model/location_model.dart';
import 'package:tratoapp/model/signature_model/signature_model.dart';

part 'signature_request.freezed.dart';
part 'signature_request.g.dart';

@freezed
abstract class SignatureRequest implements _$SignatureRequest {
  const SignatureRequest._();

  const factory SignatureRequest(
      {SignatureModel signatureCoords,
       LocationModel geolocation}) = _SignatureRequest;


  factory SignatureRequest.fromJson(Map<String, dynamic> json) =>
      _$SignatureRequestFromJson(json);

}