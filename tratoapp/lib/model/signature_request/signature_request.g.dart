// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signature_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SignatureRequest _$_$_SignatureRequestFromJson(Map<String, dynamic> json) {
  return _$_SignatureRequest(
    signatureCoords: json['signatureCoords'] == null
        ? null
        : SignatureModel.fromJson(
            json['signatureCoords'] as Map<String, dynamic>),
    geolocation: json['geolocation'] == null
        ? null
        : LocationModel.fromJson(json['geolocation'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_SignatureRequestToJson(
        _$_SignatureRequest instance) =>
    <String, dynamic>{
      'signatureCoords': instance.signatureCoords,
      'geolocation': instance.geolocation,
    };
