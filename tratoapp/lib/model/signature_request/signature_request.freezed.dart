// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'signature_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
SignatureRequest _$SignatureRequestFromJson(Map<String, dynamic> json) {
  return _SignatureRequest.fromJson(json);
}

class _$SignatureRequestTearOff {
  const _$SignatureRequestTearOff();

  _SignatureRequest call(
      {SignatureModel signatureCoords, LocationModel geolocation}) {
    return _SignatureRequest(
      signatureCoords: signatureCoords,
      geolocation: geolocation,
    );
  }
}

// ignore: unused_element
const $SignatureRequest = _$SignatureRequestTearOff();

mixin _$SignatureRequest {
  SignatureModel get signatureCoords;
  LocationModel get geolocation;

  Map<String, dynamic> toJson();
  $SignatureRequestCopyWith<SignatureRequest> get copyWith;
}

abstract class $SignatureRequestCopyWith<$Res> {
  factory $SignatureRequestCopyWith(
          SignatureRequest value, $Res Function(SignatureRequest) then) =
      _$SignatureRequestCopyWithImpl<$Res>;
  $Res call({SignatureModel signatureCoords, LocationModel geolocation});

  $SignatureModelCopyWith<$Res> get signatureCoords;
  $LocationModelCopyWith<$Res> get geolocation;
}

class _$SignatureRequestCopyWithImpl<$Res>
    implements $SignatureRequestCopyWith<$Res> {
  _$SignatureRequestCopyWithImpl(this._value, this._then);

  final SignatureRequest _value;
  // ignore: unused_field
  final $Res Function(SignatureRequest) _then;

  @override
  $Res call({
    Object signatureCoords = freezed,
    Object geolocation = freezed,
  }) {
    return _then(_value.copyWith(
      signatureCoords: signatureCoords == freezed
          ? _value.signatureCoords
          : signatureCoords as SignatureModel,
      geolocation: geolocation == freezed
          ? _value.geolocation
          : geolocation as LocationModel,
    ));
  }

  @override
  $SignatureModelCopyWith<$Res> get signatureCoords {
    if (_value.signatureCoords == null) {
      return null;
    }
    return $SignatureModelCopyWith<$Res>(_value.signatureCoords, (value) {
      return _then(_value.copyWith(signatureCoords: value));
    });
  }

  @override
  $LocationModelCopyWith<$Res> get geolocation {
    if (_value.geolocation == null) {
      return null;
    }
    return $LocationModelCopyWith<$Res>(_value.geolocation, (value) {
      return _then(_value.copyWith(geolocation: value));
    });
  }
}

abstract class _$SignatureRequestCopyWith<$Res>
    implements $SignatureRequestCopyWith<$Res> {
  factory _$SignatureRequestCopyWith(
          _SignatureRequest value, $Res Function(_SignatureRequest) then) =
      __$SignatureRequestCopyWithImpl<$Res>;
  @override
  $Res call({SignatureModel signatureCoords, LocationModel geolocation});

  @override
  $SignatureModelCopyWith<$Res> get signatureCoords;
  @override
  $LocationModelCopyWith<$Res> get geolocation;
}

class __$SignatureRequestCopyWithImpl<$Res>
    extends _$SignatureRequestCopyWithImpl<$Res>
    implements _$SignatureRequestCopyWith<$Res> {
  __$SignatureRequestCopyWithImpl(
      _SignatureRequest _value, $Res Function(_SignatureRequest) _then)
      : super(_value, (v) => _then(v as _SignatureRequest));

  @override
  _SignatureRequest get _value => super._value as _SignatureRequest;

  @override
  $Res call({
    Object signatureCoords = freezed,
    Object geolocation = freezed,
  }) {
    return _then(_SignatureRequest(
      signatureCoords: signatureCoords == freezed
          ? _value.signatureCoords
          : signatureCoords as SignatureModel,
      geolocation: geolocation == freezed
          ? _value.geolocation
          : geolocation as LocationModel,
    ));
  }
}

@JsonSerializable()
class _$_SignatureRequest extends _SignatureRequest
    with DiagnosticableTreeMixin {
  const _$_SignatureRequest({this.signatureCoords, this.geolocation})
      : super._();

  factory _$_SignatureRequest.fromJson(Map<String, dynamic> json) =>
      _$_$_SignatureRequestFromJson(json);

  @override
  final SignatureModel signatureCoords;
  @override
  final LocationModel geolocation;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SignatureRequest(signatureCoords: $signatureCoords, geolocation: $geolocation)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SignatureRequest'))
      ..add(DiagnosticsProperty('signatureCoords', signatureCoords))
      ..add(DiagnosticsProperty('geolocation', geolocation));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignatureRequest &&
            (identical(other.signatureCoords, signatureCoords) ||
                const DeepCollectionEquality()
                    .equals(other.signatureCoords, signatureCoords)) &&
            (identical(other.geolocation, geolocation) ||
                const DeepCollectionEquality()
                    .equals(other.geolocation, geolocation)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(signatureCoords) ^
      const DeepCollectionEquality().hash(geolocation);

  @override
  _$SignatureRequestCopyWith<_SignatureRequest> get copyWith =>
      __$SignatureRequestCopyWithImpl<_SignatureRequest>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SignatureRequestToJson(this);
  }
}

abstract class _SignatureRequest extends SignatureRequest {
  const _SignatureRequest._() : super._();
  const factory _SignatureRequest(
      {SignatureModel signatureCoords,
      LocationModel geolocation}) = _$_SignatureRequest;

  factory _SignatureRequest.fromJson(Map<String, dynamic> json) =
      _$_SignatureRequest.fromJson;

  @override
  SignatureModel get signatureCoords;
  @override
  LocationModel get geolocation;
  @override
  _$SignatureRequestCopyWith<_SignatureRequest> get copyWith;
}
