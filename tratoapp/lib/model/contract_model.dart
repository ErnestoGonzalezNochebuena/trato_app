import 'package:tratoapp/model/base_model.dart';

class ContractModel extends Model {
  String sId;
  bool hideAttachments;
  bool hideMobileHeader;
  bool wasNegotiated;
  bool allowAttachments;
  String jurisdiction;

  String nom151;

//  List<Null> watchers;
  Options options;
  bool blockchainTimestamp;
  String signatureType;
  String status;
  int documentId;
  bool shield3ValidationRequired;
  List<Participants> participants;
  List<Tags> tags;

//  List<Null> revision;
//  List<Null> vobo;
//  List<Null> spectators;
  String name;
  String userIsParticipant;
  String expiryStartDate;
  String expiryEndDate;
  Blockchain blockchain;
  User user;
  Type type;

//  List<Null> guests;
  List<Elements> elements;

//  List<Null> comments;
  String createdAt;
  bool alreadySent;
  Statuses statuses;
  String jwt;

  //check message property if contract is cancelled
  String message;

  int armourType;

  ContractModel(
      {this.sId,
      this.hideAttachments,
      this.hideMobileHeader,
      this.wasNegotiated,
      this.allowAttachments,
      this.jurisdiction,
//    this.watchers,
      this.options,
      this.blockchainTimestamp,
      this.signatureType,
      this.status,
      this.documentId,
      this.shield3ValidationRequired,
      this.participants,
      this.tags,
//    this.revision, this.vobo, this.spectators,
      this.name,
      this.userIsParticipant,
      this.expiryStartDate,
      this.expiryEndDate,
      this.blockchain,
      this.user,
      this.type,
//    this.guests,
      this.elements,
//    this.comments,
      this.createdAt,
      this.alreadySent,
      this.message,
      this.statuses,
      this.armourType,
      this.nom151});

  ContractModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    hideAttachments = json['hideAttachments'];
    hideMobileHeader = json['hideMobileHeader'];
    wasNegotiated = json['wasNegotiated'];
    allowAttachments = json['allowAttachments'];
    jurisdiction = json['jurisdiction'];
//    if (json['watchers'] != null) {
//      watchers = new List<Null>();
//      json['watchers'].forEach((v) { watchers.add(new Null.fromJson(v)); });
//    }
    options =
        json['options'] != null ? new Options.fromJson(json['options']) : null;
    blockchainTimestamp = json['blockchainTimestamp'];
    signatureType = json['signatureType'];
    status = json['status'];
    documentId = json['documentId'];
    shield3ValidationRequired = json['shield3ValidationRequired'];
    if (json['participants'] != null) {
      participants = new List<Participants>();
      json['participants'].forEach((v) {
        participants.add(new Participants.fromJson(v));
      });
    }
    if (json['tags'] != null) {
      tags = new List<Tags>();
      json['tags'].forEach((v) {
        tags.add(new Tags.fromJson(v));
      });
    }
//    if (json['revision'] != null) {
//      revision = new List<Null>();
//      json['revision'].forEach((v) { revision.add(new Null.fromJson(v)); });
//    }
//    if (json['vobo'] != null) {
//      vobo = new List<Null>();
//      json['vobo'].forEach((v) { vobo.add(new Null.fromJson(v)); });
//    }
//    if (json['spectators'] != null) {
//      spectators = new List<Null>();
//      json['spectators'].forEach((v) { spectators.add(new Null.fromJson(v)); });
//    }
    name = json['name'];
    userIsParticipant = json['userIsParticipant'];
    expiryStartDate = json['expiryStartDate'];
    expiryEndDate = json['expiryEndDate'];
    blockchain = json['blockchain'] != null
        ? new Blockchain.fromJson(json['blockchain'])
        : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    type = json['type'] != null ? new Type.fromJson(json['type']) : null;
//    if (json['guests'] != null) {
//      guests = new List<Null>();
//      json['guests'].forEach((v) { guests.add(new Null.fromJson(v)); });
//    }
    if (json['elements'] != null) {
      elements = new List<Elements>();
      json['elements'].forEach((v) {
        elements.add(new Elements.fromJson(v));
      });
    }
//    if (json['comments'] != null) {
//      comments = new List<Null>();
//      json['comments'].forEach((v) { comments.add(new Null.fromJson(v)); });
//    }
    createdAt = json['createdAt'];
    alreadySent = json['alreadySent'];
    message = json['message'];
    statuses = json['statuses'] != null
        ? new Statuses.fromJson(json['statuses'])
        : null;
    nom151 = json['nom151'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['hideAttachments'] = this.hideAttachments;
    data['hideMobileHeader'] = this.hideMobileHeader;
    data['wasNegotiated'] = this.wasNegotiated;
    data['allowAttachments'] = this.allowAttachments;
    data['jurisdiction'] = this.jurisdiction;
//    if (this.watchers != null) {
//      data['watchers'] = this.watchers.map((v) => v.toJson()).toList();
//    }
    if (this.options != null) {
      data['options'] = this.options.toJson();
    }
    data['blockchainTimestamp'] = this.blockchainTimestamp;
    data['signatureType'] = this.signatureType;
    data['status'] = this.status;
    data['documentId'] = this.documentId;
    data['shield3ValidationRequired'] = this.shield3ValidationRequired;
    if (this.participants != null) {
      data['participants'] = this.participants.map((v) => v.toJson()).toList();
    }
    if (this.tags != null) {
      data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
//    if (this.revision != null) {
//      data['revision'] = this.revision.map((v) => v.toJson()).toList();
//    }
//    if (this.vobo != null) {
//      data['vobo'] = this.vobo.map((v) => v.toJson()).toList();
//    }
//    if (this.spectators != null) {
//      data['spectators'] = this.spectators.map((v) => v.toJson()).toList();
//    }
    data['name'] = this.name;
    data['userIsParticipant'] = this.userIsParticipant;
    data['expiryStartDate'] = this.expiryStartDate;
    data['expiryEndDate'] = this.expiryEndDate;
    if (this.blockchain != null) {
      data['blockchain'] = this.blockchain.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
//    if (this.guests != null) {
//      data['guests'] = this.guests.map((v) => v.toJson()).toList();
//    }
    if (this.elements != null) {
      data['elements'] = this.elements.map((v) => v.toJson()).toList();
    }
//    if (this.comments != null) {
//      data['comments'] = this.comments.map((v) => v.toJson()).toList();
//    }
    data['createdAt'] = this.createdAt;
    data['alreadySent'] = this.alreadySent;
    if (this.statuses != null) {
      data['statuses'] = this.statuses.toJson();
    }
    data['nom151'] = this.nom151;

    return data;
  }
}

class Options {
  String clauseStyle;
  bool audio;
  bool video;
  bool attachments;
  bool attachmentsRequired;
  bool photo;

  Options(
      {this.clauseStyle,
      this.audio,
      this.video,
      this.attachments,
      this.attachmentsRequired,
      this.photo});

  Options.fromJson(Map<String, dynamic> json) {
    clauseStyle = json['clauseStyle'];
    audio = json['audio'];
    video = json['video'];
    attachments = json['attachments'];
    attachmentsRequired = json['attachmentsRequired'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clauseStyle'] = this.clauseStyle;
    data['audio'] = this.audio;
    data['video'] = this.video;
    data['attachments'] = this.attachments;
    data['attachmentsRequired'] = this.attachmentsRequired;
    data['photo'] = this.photo;
    return data;
  }
}

class Participants {
  bool requestBasicInformation;
  bool disableBasicInformation;
  String phone;
  bool privacyAgreement;
  bool requestAgreement;
  bool requestLocation;
  bool videoValidated;
  String video;
  bool requestSignature;
  bool signatureValidated;
  bool notify;
  int left;
  int top;
  int width;
  int height;
  bool isCurrentSigner;
  bool wasNotified;
  String sId;
  String name;
  String lastName;

//  List<Null> signatures;
//  List<Null> attachments;
  String label;
  String obligation;

//  List<Null> variables;
//  Null linkExpirationDate;
  String email;
  String status;
  String statusDate;
  PrivacyAgreementDetails privacyAgreementDetails;
  bool shield4DataRequested;

  Participants({
    this.requestBasicInformation,
    this.disableBasicInformation,
    this.phone,
    this.privacyAgreement,
    this.requestAgreement,
    this.requestLocation,
    this.videoValidated,
    this.requestSignature,
    this.signatureValidated,
    this.notify,
    this.left,
    this.top,
    this.width,
    this.height,
    this.isCurrentSigner,
    this.wasNotified,
    this.sId,
    this.name,
    this.lastName,
//    this.signatures, this.attachments,
    this.label,
    this.obligation,
//    this.variables, this.linkExpirationDate,
    this.email,
    this.status,
    this.statusDate,
    this.privacyAgreementDetails,
    this.shield4DataRequested,
  });

  Participants.fromJson(Map<String, dynamic> json) {
    requestBasicInformation = json['requestBasicInformation'];
    disableBasicInformation = json['disableBasicInformation'];
    phone = json['phone'];
    privacyAgreement = json['privacyAgreement'];
    requestAgreement = json['requestAgreement'];
    requestLocation = json['requestLocation'];
    videoValidated = json['videoValidated'];
    video = json['video'];
    requestSignature = json['requestSignature'];
    signatureValidated = json['signatureValidated'];
    notify = json['notify'];
    left = json['left'];
    top = json['top'];
    width = json['width'];
    height = json['height'];
    isCurrentSigner = json['isCurrentSigner'];
    wasNotified = json['wasNotified'];
    sId = json['_id'];
    name = json['name'];
    lastName = json['last_name'];
//    if (json['signatures'] != null) {
//      signatures = new List<Null>();
//      json['signatures'].forEach((v) { signatures.add(new Null.fromJson(v)); });
//    }
//    if (json['attachments'] != null) {
//      attachments = new List<Null>();
//      json['attachments'].forEach((v) { attachments.add(new Null.fromJson(v)); });
//    }
    label = json['label'];
    obligation = json['obligation'];
//    if (json['variables'] != null) {
//      variables = new List<Null>();
//      json['variables'].forEach((v) { variables.add(new Null.fromJson(v)); });
//    }
//    linkExpirationDate = json['linkExpirationDate'];
    email = json['email'];
    status = json['status'];
    statusDate = json['statusDate'];
    privacyAgreementDetails = json['privacyAgreementDetails'] != null
        ? new PrivacyAgreementDetails.fromJson(json['privacyAgreementDetails'])
        : null;
    shield4DataRequested = (json['shield4DataRequested'] != null)
        ? json['shield4DataRequested']
        : false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['requestBasicInformation'] = this.requestBasicInformation;
    data['disableBasicInformation'] = this.disableBasicInformation;
    data['phone'] = this.phone;
    data['privacyAgreement'] = this.privacyAgreement;
    data['requestAgreement'] = this.requestAgreement;
    data['requestLocation'] = this.requestLocation;
    data['videoValidated'] = this.videoValidated;
    data['requestSignature'] = this.requestSignature;
    data['signatureValidated'] = this.signatureValidated;
    data['notify'] = this.notify;
    data['left'] = this.left;
    data['top'] = this.top;
    data['width'] = this.width;
    data['height'] = this.height;
    data['isCurrentSigner'] = this.isCurrentSigner;
    data['wasNotified'] = this.wasNotified;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['last_name'] = this.lastName;
//    if (this.signatures != null) {
//      data['signatures'] = this.signatures.map((v) => v.toJson()).toList();
//    }
//    if (this.attachments != null) {
//      data['attachments'] = this.attachments.map((v) => v.toJson()).toList();
//    }
    data['label'] = this.label;
    data['obligation'] = this.obligation;
//    if (this.variables != null) {
//      data['variables'] = this.variables.map((v) => v.toJson()).toList();
//    }
//    data['linkExpirationDate'] = this.linkExpirationDate;
    data['email'] = this.email;
    data['status'] = this.status;
    data['statusDate'] = this.statusDate;
    if (this.privacyAgreementDetails != null) {
      data['privacyAgreementDetails'] = this.privacyAgreementDetails.toJson();
    }
    data['shield4DataRequested'] = this.shield4DataRequested;
    return data;
  }
}

class PrivacyAgreementDetails {
  String ip;
  String grantedDate;

  PrivacyAgreementDetails({this.ip, this.grantedDate});

  PrivacyAgreementDetails.fromJson(Map<String, dynamic> json) {
    ip = json['ip'];
    grantedDate = json['grantedDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ip'] = this.ip;
    data['grantedDate'] = this.grantedDate;
    return data;
  }
}

class Tags {
  bool auto;
  bool deleted;
  bool notification;

//  List<Null> daysBefore;
  String sId;
  String name;
  String value;
  String type;
  String message;
  String subtype;
  String createdAt;
  String updatedAt;

  Tags(
      {this.auto,
      this.deleted,
      this.notification,
//    this.daysBefore,
      this.sId,
      this.name,
      this.value,
      this.type,
      this.message,
      this.subtype,
      this.createdAt,
      this.updatedAt});

  Tags.fromJson(Map<String, dynamic> json) {
    auto = json['auto'];
    deleted = json['deleted'];
    notification = json['notification'];
//    if (json['daysBefore'] != null) {
//      daysBefore = new List<Null>();
//      json['daysBefore'].forEach((v) { daysBefore.add(new Null.fromJson(v)); });
//    }
    sId = json['_id'];
    name = json['name'];
    value = json['value'];
    type = json['type'];
    message = json['message'];
    subtype = json['subtype'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['auto'] = this.auto;
    data['deleted'] = this.deleted;
    data['notification'] = this.notification;
//    if (this.daysBefore != null) {
//      data['daysBefore'] = this.daysBefore.map((v) => v.toJson()).toList();
//    }
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['value'] = this.value;
    data['type'] = this.type;
    data['message'] = this.message;
    data['subtype'] = this.subtype;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class Blockchain {
  bool active;
  String sId;
  String type;

//  List<Null> originstampTimestamps;
  String createdAt;
  String updatedAt;

  Blockchain(
      {this.active,
      this.sId,
      this.type,
//    this.originstampTimestamps,
      this.createdAt,
      this.updatedAt});

  Blockchain.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    sId = json['_id'];
    type = json['type'];
//    if (json['originstampTimestamps'] != null) {
//      originstampTimestamps = new List<Null>();
//      json['originstampTimestamps'].forEach((v) { originstampTimestamps.add(new Null.fromJson(v)); });
//    }
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['active'] = this.active;
    data['_id'] = this.sId;
    data['type'] = this.type;
//    if (this.originstampTimestamps != null) {
//      data['originstampTimestamps'] = this.originstampTimestamps.map((v) => v.toJson()).toList();
//    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class User {
  String sId;
  String email;
  String familyName;
  String givenName;
  Branding branding;
  Office office;
  Plan plan;

  User(
      {this.sId,
      this.email,
      this.familyName,
      this.givenName,
      this.branding,
      this.office,
      this.plan});

  User.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    email = json['email'];
    familyName = json['familyName'];
    givenName = json['givenName'];
    branding = json['branding'] != null
        ? new Branding.fromJson(json['branding'])
        : null;
    office =
        json['office'] != null ? new Office.fromJson(json['office']) : null;
    plan = json['plan'] != null ? new Plan.fromJson(json['plan']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['email'] = this.email;
    data['familyName'] = this.familyName;
    data['givenName'] = this.givenName;
    if (this.branding != null) {
      data['branding'] = this.branding.toJson();
    }
    if (this.office != null) {
      data['office'] = this.office.toJson();
    }
    if (this.plan != null) {
      data['plan'] = this.plan.toJson();
    }
    return data;
  }
}

class Branding {
  Branding();

  Branding.fromJson(Map<String, dynamic> json) {}

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Office {
  String color;
  String logo;

  Office({this.color, this.logo});

  Office.fromJson(Map<String, dynamic> json) {
    color = json['color'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['color'] = this.color;
    data['logo'] = this.logo;
    return data;
  }
}

class Plan {
  String name;
  String validUntil;
  String type;
  int users;
  int contracts;
  int chargeDay;

  Plan(
      {this.name,
      this.validUntil,
      this.type,
      this.users,
      this.contracts,
      this.chargeDay});

  Plan.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    validUntil = json['validUntil'];
    type = json['type'];
    users = json['users'];
    contracts = json['contracts'];
    chargeDay = json['chargeDay'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['validUntil'] = this.validUntil;
    data['type'] = this.type;
    data['users'] = this.users;
    data['contracts'] = this.contracts;
    data['chargeDay'] = this.chargeDay;
    return data;
  }
}

class Type {
  String sId;
  String name;

  Type({this.sId, this.name});

  Type.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    return data;
  }
}

class Elements {
  Branding smartclause;
  bool locked;
  bool isAccepted;
  bool isNegotiable;
  bool isNonVotable;
  int currentVersion;
  bool restart;
  bool checked;
  String sId;

//  List<Null> signComments;
//  List<Null> reviewComments;
//  List<Null> versions;
//  List<Null> reviewers;
  int number;
  String type;
  String content;
  int block;
  int index;

//  List<Null> comments;
  String createdAt;
  String updatedAt;
  bool staticfield;

  Elements(
      {this.smartclause,
      this.locked,
      this.isAccepted,
      this.isNegotiable,
      this.isNonVotable,
      this.currentVersion,
      this.restart,
      this.checked,
      this.sId,
//    this.signComments, this.reviewComments, this.versions, this.reviewers,
      this.number,
      this.type,
      this.content,
      this.block,
      this.index,
//    this.comments,
      this.createdAt,
      this.updatedAt,
      this.staticfield});

  Elements.fromJson(Map<String, dynamic> json) {
    smartclause = json['smartclause'] != null
        ? new Branding.fromJson(json['smartclause'])
        : null;
    locked = json['locked'];
    isAccepted = json['isAccepted'];
    isNegotiable = json['isNegotiable'];
    isNonVotable = json['isNonVotable'];
    currentVersion = json['currentVersion'];
    restart = json['restart'];
    checked = json['checked'];
    sId = json['_id'];
//    if (json['signComments'] != null) {
//      signComments = new List<Null>();
//      json['signComments'].forEach((v) { signComments.add(new Null.fromJson(v)); });
//    }
//    if (json['reviewComments'] != null) {
//      reviewComments = new List<Null>();
//      json['reviewComments'].forEach((v) { reviewComments.add(new Null.fromJson(v)); });
//    }
//    if (json['versions'] != null) {
//      versions = new List<Null>();
//      json['versions'].forEach((v) { versions.add(new Null.fromJson(v)); });
//    }
//    if (json['reviewers'] != null) {
//      reviewers = new List<Null>();
//      json['reviewers'].forEach((v) { reviewers.add(new Null.fromJson(v)); });
//    }
    number = json['number'];
    type = json['type'];
    content = json['content'];
    block = json['block'];
    index = json['index'];
//    if (json['comments'] != null) {
//      comments = new List<Null>();
//      json['comments'].forEach((v) { comments.add(new Null.fromJson(v)); });
//    }
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    staticfield = json['staticfield'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.smartclause != null) {
      data['smartclause'] = this.smartclause.toJson();
    }
    data['locked'] = this.locked;
    data['isAccepted'] = this.isAccepted;
    data['isNegotiable'] = this.isNegotiable;
    data['isNonVotable'] = this.isNonVotable;
    data['currentVersion'] = this.currentVersion;
    data['restart'] = this.restart;
    data['checked'] = this.checked;
    data['_id'] = this.sId;
//    if (this.signComments != null) {
//      data['signComments'] = this.signComments.map((v) => v.toJson()).toList();
//    }
//    if (this.reviewComments != null) {
//      data['reviewComments'] = this.reviewComments.map((v) => v.toJson()).toList();
//    }
//    if (this.versions != null) {
//      data['versions'] = this.versions.map((v) => v.toJson()).toList();
//    }
//    if (this.reviewers != null) {
//      data['reviewers'] = this.reviewers.map((v) => v.toJson()).toList();
//    }
    data['number'] = this.number;
    data['type'] = this.type;
    data['content'] = this.content;
    data['block'] = this.block;
    data['index'] = this.index;
//    if (this.comments != null) {
//      data['comments'] = this.comments.map((v) => v.toJson()).toList();
//    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['staticfield'] = this.staticfield;
    return data;
  }
}

class Statuses {
  bool isDocument;
  bool isMyContract;
  bool amIParticipant;
  bool isNegotiable;
  bool wasNegotiated;
  Participants participant;
  bool haveToSign;
  bool isMyTurn;
  bool hasMissingVobos;

//  Null isTratoUser;
  bool iHaveAccepted;
  bool everyOneAccepted;
  String agreementMessage;

//  List<Null> variablesMissing;
//  List<Null> attachmentsMissing;

  Statuses({
    this.isDocument,
    this.isMyContract,
    this.amIParticipant,
    this.isNegotiable,
    this.wasNegotiated,
    this.participant,
    this.haveToSign,
    this.isMyTurn,
    this.hasMissingVobos,
//    this.isTratoUser,
    this.iHaveAccepted,
    this.everyOneAccepted,
    this.agreementMessage,
//    this.variablesMissing, this.attachmentsMissing
  });

  Statuses.fromJson(Map<String, dynamic> json) {
    isDocument = json['isDocument'];
    isMyContract = json['isMyContract'];
    amIParticipant = json['amIParticipant'];
    isNegotiable = json['isNegotiable'];
    wasNegotiated = json['wasNegotiated'];
    participant = json['participant'] != null
        ? new Participants.fromJson(json['participant'])
        : null;
    haveToSign = json['haveToSign'];
    isMyTurn = json['isMyTurn'];
    hasMissingVobos = json['hasMissingVobos'];
//    isTratoUser = json['isTratoUser'];
    iHaveAccepted = json['iHaveAccepted'];
    everyOneAccepted = json['everyOneAccepted'];
    agreementMessage = json['agreementMessage'];
//    if (json['variablesMissing'] != null) {
//      variablesMissing = new List<Null>();
//      json['variablesMissing'].forEach((v) { variablesMissing.add(new Null.fromJson(v)); });
//    }
//    if (json['attachmentsMissing'] != null) {
//      attachmentsMissing = new List<Null>();
//      json['attachmentsMissing'].forEach((v) { attachmentsMissing.add(new Null.fromJson(v)); });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isDocument'] = this.isDocument;
    data['isMyContract'] = this.isMyContract;
    data['amIParticipant'] = this.amIParticipant;
    data['isNegotiable'] = this.isNegotiable;
    data['wasNegotiated'] = this.wasNegotiated;
    if (this.participant != null) {
      data['participant'] = this.participant.toJson();
    }
    data['haveToSign'] = this.haveToSign;
    data['isMyTurn'] = this.isMyTurn;
    data['hasMissingVobos'] = this.hasMissingVobos;
//    data['isTratoUser'] = this.isTratoUser;
    data['iHaveAccepted'] = this.iHaveAccepted;
    data['everyOneAccepted'] = this.everyOneAccepted;
    data['agreementMessage'] = this.agreementMessage;
//    if (this.variablesMissing != null) {
//      data['variablesMissing'] = this.variablesMissing.map((v) => v.toJson()).toList();
//    }
//    if (this.attachmentsMissing != null) {
//      data['attachmentsMissing'] = this.attachmentsMissing.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}
