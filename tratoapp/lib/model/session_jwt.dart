import 'dart:collection';

import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/utils/utilities.dart';

class SessionJwt extends Model {
  bool success;
  String jwt;
  String error;

  SessionJwt();

  factory SessionJwt.fromJson(Map<String, dynamic> json) {
    var session = SessionJwt();
    session.jwt = json["jwt"];
    session.success = json["success"];
    session.error = json["error"];
    print(session.jwt);
    return session;
  }

  Map<String, dynamic> toJson(String path){
    Map<String, String> map = new HashMap();
    map["uuid"] = Utilities().getUuid(path);
    map["contractId"] = Utilities().getContractId(path);
    return map;
  }
}
