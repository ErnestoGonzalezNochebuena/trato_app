import 'package:tratoapp/model/base_model.dart';

class RelateVideoModel extends Model {
  bool success;

  RelateVideoModel();

  factory RelateVideoModel.fromJson(Map<String, dynamic> json) {
    RelateVideoModel relateVideoModel = RelateVideoModel();
    relateVideoModel.success = json["success"];
    return relateVideoModel;
  }

  Map<String, String> toJson(
      String url) {
    return {
      "video": url,
    };
  }

}
