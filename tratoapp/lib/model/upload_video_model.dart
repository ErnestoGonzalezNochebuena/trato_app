import 'package:flutter/cupertino.dart';
import 'package:tratoapp/model/base_model.dart';

class UploadVideoModel extends Model {
  bool success;
  String url;

  UploadVideoModel();

  factory UploadVideoModel.fromJson(Map<String, dynamic> json) {
    UploadVideoModel uploadVideoModel = UploadVideoModel();
    uploadVideoModel.success = (json['file'] != null);
    uploadVideoModel.url = json['file'];
    return uploadVideoModel;
  }

  static Map<String, String> toJson(String video) {
    return {
      "data": "data:video/webm;base64,$video",
      "name": "video.webm",
      "type": "video/webm"
    };
  }
}
