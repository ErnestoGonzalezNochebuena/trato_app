import 'base_model.dart';

class ApiResponse {
  //ype 'List<dynamic>' is not a subtype of type 'Map<String, dynamic>'
  String methodCalled = "";
  Model model;
  List<Model> models;
  String unprocessableEntityMessage;
  Map<String, dynamic> unprocessableEntityJson;
  String error;

  Map<String, dynamic> responseJson;
  List<dynamic> responseJsonList;

  ApiResponse();
}
