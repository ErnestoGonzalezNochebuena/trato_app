import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:tratoapp/model/coord_model/coord_model.dart';
import 'package:tratoapp/model/signature_model/signature_model.dart';

part 'location_model.freezed.dart';
part 'location_model.g.dart';

@freezed
abstract class LocationModel implements _$LocationModel {
  const LocationModel._();

  const factory LocationModel(
      {dynamic latitude,
        dynamic  longitude}) = _LocationModel;


  factory LocationModel.fromJson(Map<String, dynamic> json) =>
      _$LocationModelFromJson(json);

}