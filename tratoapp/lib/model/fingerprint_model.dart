import 'base_model.dart';

class FingerprintModel extends Model {
  Data _data;

  FingerprintModel();

  factory FingerprintModel.fromJson(Map<String, dynamic> json) {
    FingerprintModel model = FingerprintModel();
    model._data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    return model;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }
}

class Data extends Model {
  Fingerprint _rightthumb;
  Fingerprint _rightring;
  Fingerprint _leftindex;
  Fingerprint _rightlittle;
  Fingerprint _rightmiddle;
  Fingerprint _leftlittle;
  Fingerprint _rightindex;
  Fingerprint _leftring;
  Fingerprint _leftthumb;
  Fingerprint _leftmiddle;

  Data(
      {Fingerprint rightthumb,
      Fingerprint rightring,
      Fingerprint leftindex,
      Fingerprint rightlittle,
      Fingerprint rightmiddle,
      Fingerprint leftlittle,
      Fingerprint rightindex,
      Fingerprint leftring,
      Fingerprint leftthumb,
      Fingerprint leftmiddle}) {
    this._rightthumb = rightthumb;
    this._rightring = rightring;
    this._leftindex = leftindex;
    this._rightlittle = rightlittle;
    this._rightmiddle = rightmiddle;
    this._leftlittle = leftlittle;
    this._rightindex = rightindex;
    this._leftring = leftring;
    this._leftthumb = leftthumb;
    this._leftmiddle = leftmiddle;
  }

  Fingerprint get rightthumb => _rightthumb;

  set rightthumb(Fingerprint rightthumb) => _rightthumb = rightthumb;

  Fingerprint get rightring => _rightring;

  set rightring(Fingerprint rightring) => _rightring = rightring;

  Fingerprint get leftindex => _leftindex;

  set leftindex(Fingerprint leftindex) => _leftindex = leftindex;

  Fingerprint get rightlittle => _rightlittle;

  set rightlittle(Fingerprint rightlittle) => _rightlittle = rightlittle;

  Fingerprint get rightmiddle => _rightmiddle;

  set rightmiddle(Fingerprint rightmiddle) => _rightmiddle = rightmiddle;

  Fingerprint get leftlittle => _leftlittle;

  set leftlittle(Fingerprint leftlittle) => _leftlittle = leftlittle;

  Fingerprint get rightindex => _rightindex;

  set rightindex(Fingerprint rightindex) => _rightindex = rightindex;

  Fingerprint get leftring => _leftring;

  set leftring(Fingerprint leftring) => _leftring = leftring;

  Fingerprint get leftthumb => _leftthumb;

  set leftthumb(Fingerprint leftthumb) => _leftthumb = leftthumb;

  Fingerprint get leftmiddle => _leftmiddle;

  set leftmiddle(Fingerprint leftmiddle) => _leftmiddle = leftmiddle;

  Data.fromJson(Map<String, dynamic> json) {
    _rightthumb = json['rightthumb'] != null
        ? new Fingerprint.fromJson(json['rightthumb'])
        : null;
    _rightring = json['rightring'] != null
        ? new Fingerprint.fromJson(json['rightring'])
        : null;
    _leftindex = json['leftindex'] != null
        ? new Fingerprint.fromJson(json['leftindex'])
        : null;
    _rightlittle = json['rightlittle'] != null
        ? new Fingerprint.fromJson(json['rightlittle'])
        : null;
    _rightmiddle = json['rightmiddle'] != null
        ? new Fingerprint.fromJson(json['rightmiddle'])
        : null;
    _leftlittle = json['leftlittle'] != null
        ? new Fingerprint.fromJson(json['leftlittle'])
        : null;
    _rightindex = json['rightindex'] != null
        ? new Fingerprint.fromJson(json['rightindex'])
        : null;
    _leftring = json['leftring'] != null
        ? new Fingerprint.fromJson(json['leftring'])
        : null;
    _leftthumb = json['leftthumb'] != null
        ? new Fingerprint.fromJson(json['leftthumb'])
        : null;
    _leftmiddle = json['leftmiddle'] != null
        ? new Fingerprint.fromJson(json['leftmiddle'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._rightthumb != null) {
      data['rightthumb'] = this._rightthumb.toJson();
    }
    if (this._rightring != null) {
      data['rightring'] = this._rightring.toJson();
    }
    if (this._leftindex != null) {
      data['leftindex'] = this._leftindex.toJson();
    }
    if (this._rightlittle != null) {
      data['rightlittle'] = this._rightlittle.toJson();
    }
    if (this._rightmiddle != null) {
      data['rightmiddle'] = this._rightmiddle.toJson();
    }
    if (this._leftlittle != null) {
      data['leftlittle'] = this._leftlittle.toJson();
    }
    if (this._rightindex != null) {
      data['rightindex'] = this._rightindex.toJson();
    }
    if (this._leftring != null) {
      data['leftring'] = this._leftring.toJson();
    }
    if (this._leftthumb != null) {
      data['leftthumb'] = this._leftthumb.toJson();
    }
    if (this._leftmiddle != null) {
      data['leftmiddle'] = this._leftmiddle.toJson();
    }
    return data;
  }
}

class Fingerprint extends Model {
  String _date;
  String _hand;
  String _finger;
  int _identyQuality;
  int _height;
  int _width;
  int _bitsPerPixel;
  String _resolution;
  String _spoofScore;
  Templates _templates;

  Fingerprint(
      {String date,
      String hand,
      String finger,
      int identyQuality,
      int height,
      int width,
      int bitsPerPixel,
      String resolution,
      String spoofScore,
      Templates templates}) {
    this._date = date;
    this._hand = hand;
    this._finger = finger;
    this._identyQuality = identyQuality;
    this._height = height;
    this._width = width;
    this._bitsPerPixel = bitsPerPixel;
    this._resolution = resolution;
    this._spoofScore = spoofScore;
    this._templates = templates;
  }

  String get date => _date;

  set date(String date) => _date = date;

  String get hand => _hand;

  set hand(String hand) => _hand = hand;

  String get finger => _finger;

  set finger(String finger) => _finger = finger;

  int get identyQuality => _identyQuality;

  set identyQuality(int identyQuality) => _identyQuality = identyQuality;

  int get height => _height;

  set height(int height) => _height = height;

  int get width => _width;

  set width(int width) => _width = width;

  int get bitsPerPixel => _bitsPerPixel;

  set bitsPerPixel(int bitsPerPixel) => _bitsPerPixel = bitsPerPixel;

  String get resolution => _resolution;

  set resolution(String resolution) => _resolution = resolution;

  String get spoofScore => _spoofScore;

  set spoofScore(String spoofScore) => _spoofScore = spoofScore;

  Templates get templates => _templates;

  set templates(Templates templates) => _templates = templates;

  Fingerprint.fromJson(Map<String, dynamic> json) {
    _date = json['date'];
    _hand = json['hand'];
    _finger = json['finger'];
    _identyQuality = json['identy_quality'];
    _height = json['height'];
    _width = json['width'];
    _bitsPerPixel = json['bits_per_pixel'];
    _resolution = json['resolution'];
    _spoofScore = json['spoof_score'];
    _templates = json['templates'] != null
        ? new Templates.fromJson(json['templates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this._date;
    data['hand'] = this._hand;
    data['finger'] = this._finger;
    data['identy_quality'] = this._identyQuality;
    data['height'] = this._height;
    data['width'] = this._width;
    data['bits_per_pixel'] = this._bitsPerPixel;
    data['resolution'] = this._resolution;
    data['spoof_score'] = this._spoofScore;
    if (this._templates != null) {
      data['templates'] = this._templates.toJson();
    }
    return data;
  }
}

class Templates extends Model {
  String _wSQ;

  Templates({String wSQ}) {
    this._wSQ = wSQ;
  }

  String get wSQ => _wSQ;

  set wSQ(String wSQ) => _wSQ = wSQ;

  Templates.fromJson(Map<String, dynamic> json) {
    _wSQ = json['WSQ'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['WSQ'] = this._wSQ;
    return data;
  }
}
