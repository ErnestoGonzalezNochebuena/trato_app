import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/utils/colors.dart';

class MainCustomCard extends StatelessWidget {
  const MainCustomCard(
      {Key key,
      this.icon,
      this.title,
      this.content,
      this.isLoading = false,
      this.footer})
      : super(key: key);

  final String icon;
  final String title;
  final String content;
  final bool isLoading;
  final Widget footer;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
            padding: EdgeInsets.only(left: 30, right: 30, bottom: 95, top: 95),
            child: Card(
                elevation: 20.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    icon.isNotEmpty
                        ? Image.asset(
                            icon,
                            width: 150,
                            height: 150,
                          )
                        : Empty(),
                    SizedBox(height: 10.0),
                    Padding(
                        padding: EdgeInsets.all(10.0),
                        child: CustomText(
                            customText: title,
                            fontSize: 18,
                            fontWeight: FontWeight.w500)),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                        child: CustomText(
                            customText: content,
                            fontSize: 14,
                            align: TextAlign.start,
                            textColor: Colors.black54)),
                    SizedBox(
                      height: 20,
                    ),
                    isLoading
                        ? Padding(
                            padding: EdgeInsets.only(bottom: 20.0, top: 10.0),
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  TratoColors.orange),
                            ))
                        : Empty(),
                    footer != null
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    left: 10.0,
                                    right: 10.0,
                                    top: 60.0,
                                    bottom: 20.0),
                                child: footer))
                        : Empty()
                  ],
                ))));
  }
}
