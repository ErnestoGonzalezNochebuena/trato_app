import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tratoapp/components/empty_view.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({this.onBackPressed, this.allowBack = false, Key key})
      : super(key: key);

  final VoidCallback onBackPressed;
  final bool allowBack;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[_buildBackArrow(), _buildLogo()],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();

  _buildBackArrow() {
    return allowBack
        ? Container(
            child: IconButton(
              icon: Icon(CupertinoIcons.back),
              onPressed: onBackPressed,
            ),
          )
        : Empty();
  }

  _buildLogo() {
    return Empty();
  }
}
