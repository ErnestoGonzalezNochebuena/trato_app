import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tratoapp/utils/colors.dart';

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final IconData iconData;

  const CircleButton({Key key, this.onTap, this.iconData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 50.0;

    return new InkWell(
      onTap: onTap,
      child: new Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: TratoColors.orange,
          shape: BoxShape.circle,
        ),
        child: Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    );
  }
}
