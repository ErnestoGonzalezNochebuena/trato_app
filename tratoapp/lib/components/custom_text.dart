import 'package:flutter/material.dart';

//It builds text widget specifying its text and its styling

class CustomText extends StatelessWidget {
  final String customText;
  final Color textColor;
  final double fontSize;
  final String fontFamily;
  final FontWeight fontWeight;
  final TextAlign align;
  final int maxLines;
  CustomText(
      {@required this.customText,
      this.textColor = Colors.black,
      this.fontSize = 16.0,
      this.fontFamily = "Futura",
      this.fontWeight = FontWeight.normal,
      this.align = TextAlign.center,
      this.maxLines = 50});

  @override
  Widget build(BuildContext context) {
    return Text(
      customText,
      textAlign: align,
      maxLines: maxLines,
      style: TextStyle(
          fontStyle: FontStyle.normal,
          color: textColor,
          fontSize: fontSize,
          fontFamily: fontFamily,
          fontWeight: fontWeight),
    );
  }
}
