import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class ApiServiceProvider {
  static Future<String> loadPDF(String repoPdfUrl, bool isNom151) async {
    var response = await http.get(repoPdfUrl);

    var dir = await getApplicationDocumentsDirectory();
    var path = "";
    if(isNom151){
      path = "${dir.path}/Nom151.pdf";
    }else{
      path = "${dir.path}/Contrato.pdf";
    }
    File file = new File(path);
    file.writeAsBytesSync(response.bodyBytes, flush: true);
    return file.path;
  }
}
