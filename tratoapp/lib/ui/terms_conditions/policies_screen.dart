import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class PoliciesScreen extends StatefulWidget {
  final int informationType;

  const PoliciesScreen({@required this.informationType});

  static const int TermsAndConditions = 0;
  static const int PrivacyPolicy = 1;

  @override
  _PoliciesScreenState createState() => _PoliciesScreenState();
}

class _PoliciesScreenState extends State<PoliciesScreen> with AfterLayoutMixin {
  @override
  void afterFirstLayout(BuildContext context) {}

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          ExtendedNavigator.rootNavigator.pop();
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            leading: IconButton(
                iconSize: 40,
                icon: Icon(CupertinoIcons.back),
                color: Colors.black54,
                onPressed: () => ExtendedNavigator.rootNavigator.pop()),
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: Padding(
              padding:
                  EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              child: Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  )),
                  child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Empty(),
//                        Html(
//                          data: widget.informationType ==
//                                  PoliciesScreen.TermsAndConditions
//                              ? StringConstants.termsAndConditionsUrl
//                              : StringConstants.privacyPolicyUrl,
//                        )
                      ])))),
          bottomNavigationBar: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                        padding: EdgeInsets.only(
                            left: 30, right: 30, top: 20, bottom: 40),
                        child: NiceButton(
                          elevation: 8,
                          radius: 8,
                          background: TratoColors.orange,
                          text: "Regresar",
                          fontSize: 18,
                          onPressed: () {
                            ExtendedNavigator.rootNavigator.pop();
                          },
                        )))
              ])),
        ));
  }
}
