import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/accept_agreement_manager.dart';
import 'package:tratoapp/manager/get_contract_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/session_jwt.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/ui/contracts/contract_screen.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';
import 'package:url_launcher/url_launcher.dart';

class AcceptConditions extends StatefulWidget {
  final ContractModel contractModel;

  AcceptConditions({@required this.contractModel});

  @override
  _AcceptConditionsState createState() => _AcceptConditionsState();
}

class _AcceptConditionsState extends State<AcceptConditions>
    with AfterLayoutMixin {
  bool loading = false;

  static const int TermsAndConditions = 0;
  static const int PrivacyPolicy = 1;

  final BehaviorSubject<ApiResponse> _startContract =
      BehaviorSubject<ApiResponse>();

  BehaviorSubject<ApiResponse> _subjectContract =
      BehaviorSubject<ApiResponse>();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _startContract.close();
    _subjectContract.close();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
              Routes.contractListScreen, (route) => false,
              arguments: ContractListScreenArguments(isBackFromContract: true));
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            leading: IconButton(
              iconSize: 40,
              icon: Icon(CupertinoIcons.back),
              color: Colors.black54,
              onPressed: () => ExtendedNavigator.rootNavigator
                  .pushNamedAndRemoveUntil(
                      Routes.contractListScreen, (route) => false,
                      arguments: ContractListScreenArguments(
                          isBackFromContract: true)),
            ),
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: Padding(
              padding:
                  EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              child: Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  )),
                  child: ListView(
//                      child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          mainAxisSize: MainAxisSize.max,
//                          mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                            padding:
                                EdgeInsets.only(top: 20.0, right: 10, left: 10),
                            child: CustomText(
                                align: TextAlign.center,
                                customText: "Bienvenido",
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                textColor: Colors.black54)),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: CustomText(
                                customText:
                                    "Has recibido un contrato.\nPor favor revisa las acciones que puedes realizar",
                                fontSize: 15,
                                align: TextAlign.center,
                                textColor: Colors.black54)),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                Image.asset(
                                  ImageConstants.checkIcon,
                                  width: 20,
                                  height: 20,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                CustomText(
                                    customText:
                                        "Revisa detenidamente el contrato",
                                    fontSize: 15,
                                    align: TextAlign.justify,
                                    textColor: Colors.black54)
                              ],
                            )),
                        SizedBox(
                          height: 5.0,
                        ),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                Image.asset(
                                  ImageConstants.checkIcon,
                                  width: 20,
                                  height: 20,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                    child: RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                        text:
                                            "Para realizar el proceso correcto de la firma de tu contrato sigue las instrucciones que aparecerán en tu pantalla durante todo el proceso.",
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black54))
                                  ]),
                                ))
                              ],
                            )),
                        SizedBox(
                          height: 5.0,
                        ),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                SizedBox(
                                  width: 10,
                                ),
                                Image.asset(
                                  ImageConstants.checkIcon,
                                  width: 20,
                                  height: 20,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                CustomText(
                                    customText: "Acepta el aviso de privacidad",
                                    fontSize: 15,
                                    align: TextAlign.justify,
                                    textColor: Colors.black54)
                              ],
                            )),
                        SizedBox(
                          height: 5.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(children: [
                                TextSpan(
                                    text: "Al dar clic en el botón ",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black54)),
                                TextSpan(
                                    text: "Comienza la experiencia ",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold)),
                                TextSpan(
                                    text: "estás aceptando nuestros ",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black54)),
                                TextSpan(
                                    text: "Términos y condiciones.",
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        this._launchURL(PrivacyPolicy);
                                      },
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: TratoColors.orange)),
                                TextSpan(
                                    text: "y reconoces que ",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black54)),
                                TextSpan(
                                    text: "TRATO ",
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold)),
                                TextSpan(
                                    text:
                                        "procesará mi información personal de acuerdo con el ",
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black54)),
                                TextSpan(
                                    text: "Aviso de privacidad",
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        this._launchURL(PrivacyPolicy);
                                      },
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: TratoColors.orange))
                              ])),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                      ]))),
          bottomNavigationBar: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 20, bottom: 40),
                      child: !loading
                          ? NiceButton(
                              elevation: 8,
                              radius: 8,
                              width: double.infinity,
                              background: TratoColors.orange,
                              text: "Comienza la experiencia",
                              fontSize: 14,
                              onPressed: () {
                                this.setState(() {
                                  this.loading = true;
                                });
                                acceptAgreements();
                              },
                            )
                          : Center(
                              child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  TratoColors.orange),
                            )),
                    ))
              ])),
        ));
  }

  void acceptAgreements() {
    AcceptAgreementManager().apiRequest(
        _startContract,
        HttpMethods.POST,
        {},
        null,
        Urls.acceptAgreement + "${widget.contractModel.sId}/acceptagreement",
        (json) => SessionJwt.fromJson(json));
  }

  @override
  void afterFirstLayout(BuildContext context) {
    this._startContract.listen((apiResponse) {
      SessionJwt response = apiResponse.model;
      if (response.success) {
        GetContractManager()
            .show(_subjectContract, widget.contractModel.sId, {});
      } else {
        ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
            Routes.contractListScreen, (route) => false,
            arguments: ContractListScreenArguments(isBackFromContract: true));
        Navigation.showToast(response.error, context);
      }
    });
    this._subjectContract.stream.listen((apiResponse) {
      this.setState(() {
        loading = false;
      });
      if (apiResponse.model != null) {
        ContractModel model = apiResponse.model;
//        GetContractManager().addContract(model);
//         if (model.statuses.participant.requestAgreement) {
// //          GetContractManager().setCurrentContract(model);
//           ExtendedNavigator.rootNavigator.pushNamed(Routes.contractScreen,
//               arguments: ContractScreenArguments(
//                   isSignPreview: false, contractModel: model));
//         } else {
        ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
            Routes.contractScreen, (route) => false,
            arguments: ContractScreenArguments(
                contractModel: model, isSignPreview: false));
        // Navigation.showToast(StringConstants.armour3_no_available, context);
        // }
      } else {
        ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
            Routes.contractListScreen, (route) => false,
            arguments: ContractListScreenArguments(isBackFromContract: true));
        Navigation.showToast(StringConstants.contract_no_available, context);
      }
    });
  }

  _launchURL(int informationType) async {
    String url;
    if (informationType == TermsAndConditions) {
      url = StringConstants.termsAndConditionsUrl;
    } else {
      url = StringConstants.privacyPolicyUrl;
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
