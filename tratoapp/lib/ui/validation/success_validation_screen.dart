import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:open_file/open_file.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/show_contract_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/pdf_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/services/pdf_service_provider.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class SuccessValidationScreen extends StatefulWidget {
  final ContractModel contractModel;

  const SuccessValidationScreen({@required this.contractModel});

  @override
  _SuccessValidationState createState() => _SuccessValidationState();
}

class _SuccessValidationState extends State<SuccessValidationScreen>
    with AfterLayoutMixin {
  String urlPDFPath;
  bool _loading = false;
  String localPath;

  String _openResult = 'Unknown';

  Future<void> openFile() async {
    final result = await OpenFile.open(localPath);
    ExtendedNavigator.rootNavigator
        .pushNamedAndRemoveUntil(Routes.contractListScreen, (route) => false);
    setState(() {
      _openResult = "type=${result.type}  message=${result.message}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
              Routes.contractListScreen, (route) => false,
              arguments: ContractListScreenArguments(isBackFromContract: true));
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: SingleChildScrollView(
              child: MainCustomCard(
            icon: ImageConstants.checkIcon,
            title: this.getTitle(),
            content: this.getContent(),
            isLoading: _loading,
          )),
          bottomNavigationBar: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  )),
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: 30, right: 30, top: 20, bottom: 40),
                      child: Column(children: [
                        widget.contractModel.nom151 != null
                            ? NiceButton(
                                elevation: 8,
                                radius: 8,
                                background: TratoColors.orange,
                                text: StringConstants.downloadNom151,
                                fontSize: 16,
                                onPressed: () =>
                                    _getPdf(widget.contractModel.nom151,true),
                              )
                            : Empty(),
                        widget.contractModel.nom151 != null
                            ? SizedBox(
                                height: 10,
                              )
                            : Empty(),
                        NiceButton(
                          elevation: 8,
                          radius: 8,
                          background: TratoColors.orange,
                          text: widget.contractModel.shield3ValidationRequired
                              ? StringConstants.next
                              : StringConstants.downloadContract,
                          fontSize: 16,
                          onPressed: () =>
                              widget.contractModel.shield3ValidationRequired
                                  ? ExtendedNavigator.rootNavigator.pushNamed(
                                      Routes.contractScreen,
                                      arguments: ContractScreenArguments(
                                          isSignPreview: true,
                                          contractModel: null))
                                  : _getPdf(urlPDFPath, false),
                        ),
                      ])),
                )
              ])),
        ));
  }

  Future<void> _getPdf(String urlPath, bool isNOM151) async {
    ApiServiceProvider.loadPDF(urlPath,isNOM151).then((value) {
      setState(() {
        localPath = value;
        this._loading = false;
        openFile();
      });
    });

    this.setState(() {
      this._loading = true;
    });
  }

  final BehaviorSubject<ApiResponse> _subjectContractScreen =
      BehaviorSubject<ApiResponse>();

  @override
  void afterFirstLayout(BuildContext context) {
    ShowContractManager()
        .show(_subjectContractScreen, widget.contractModel.sId, {});
    _subjectContractScreen.listen((apiResponse) {
      if (apiResponse.model != null) {
        PdfModel model = apiResponse.model;
        urlPDFPath = model.url;
        _initPdf();
      }
    });
  }

  Future<void> _initPdf() async {
    ApiServiceProvider.loadPDF(urlPDFPath,false).then((value) {
      setState(() {
        localPath = value;
        this._loading = false;
      });
    });

    this.setState(() {
      this._loading = true;
    });
  }

  String getTitle() {
    if(widget.contractModel.status == StringConstants.status038 ||
        widget.contractModel.status == StringConstants.status027){
      return StringConstants.b1SuccessTitle;
    }else {
      return !widget.contractModel.statuses.participant.requestAgreement
          ? StringConstants.titleB2Success
          : StringConstants.title_success_validation;
    }
  }

  String getContent() {
    if(widget.contractModel.status == StringConstants.status038 ||
        widget.contractModel.status == StringConstants.status027){
      return StringConstants.b1SuccessContent;
    }else {
      return !widget.contractModel.statuses.participant.requestAgreement
          ? StringConstants.contentB2Success
          : widget.contractModel.shield3ValidationRequired
          ? StringConstants.content_success_validation
          : StringConstants.content_success_validation2;
    }
  }
}
