import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_pusher/pusher.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/application_settings.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class WaitingValidationScreen extends StatefulWidget {
  final ContractModel contractModel;

  const WaitingValidationScreen({@required this.contractModel});

  @override
  _WaitingValidationState createState() => _WaitingValidationState();
}

class _WaitingValidationState extends State<WaitingValidationScreen> {
  Channel _channel;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return;
      },
      child: Scaffold(
        appBar: AppBar(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20.0),
            bottomRight: Radius.circular(20.0),
          )),
          backgroundColor: Colors.white,
          actions: [
            Padding(
                padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                child: Image.asset(
                  ImageConstants.mainLogo,
                  height: 30.0,
                  width: 120.0,
                ))
          ],
        ),
        body: SingleChildScrollView(
            child: MainCustomCard(
          icon: ImageConstants.clockIcon,
          title: StringConstants.title_waiting_validation,
          content: StringConstants.content_waiting_validation,
          isLoading: true,
        )),
        bottomNavigationBar: SafeArea(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
              Card(
                elevation: 20.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                )),
                child: Padding(
                  padding:
                      EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 40),
                  child: NiceButton(
                    elevation: 8,
                    radius: 8,
                    background: TratoColors.orange,
                    text: StringConstants.next,
                    fontSize: 18,
                    onPressed: () {
                      ExtendedNavigator.rootNavigator.pushNamed(
                          Routes.contractListScreen,
                          arguments: ContractListScreenArguments(
                              isBackFromContract: true));
                    },
                  ),
                ),
              )
            ])),
      ),
    );
  }

}
