import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:rxdart/rxdart.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';

class FingerprintScreen extends StatefulWidget {
  final ContractModel contractModel;

  const FingerprintScreen({@required this.contractModel});

  @override
  _FingerprintState createState() => _FingerprintState();
}

class _FingerprintState extends State<FingerprintScreen> {
  static const platform = const MethodChannel(
      '${StringConstants.appChannelName}${StringConstants.fingerIdPlugin}');

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
              Routes.contractListScreen, (route) => false,
              arguments: ContractListScreenArguments(isBackFromContract: true));
        },
        child: Scaffold(
            appBar: AppBar(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              )),
              leading: IconButton(
                iconSize: 40,
                icon: Icon(CupertinoIcons.back),
                color: Colors.black54,
                onPressed: () {
                  ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
                      Routes.contractListScreen, (route) => false,
                      arguments: ContractListScreenArguments(
                          isBackFromContract: true));
                },
              ),
              backgroundColor: Colors.white,
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                    child: Image.asset(
                      ImageConstants.mainLogo,
                      height: 30.0,
                      width: 120.0,
                    ))
              ],
            ),
            bottomNavigationBar: SafeArea(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                  Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 30, right: 30, top: 20, bottom: 40),
                      child: !isLoading
                          ? NiceButton(
                              elevation: 8,
                              radius: 8,
                              background: TratoColors.orange,
                              text: "Tomar huellas",
                              fontSize: 18,
                              onPressed: () {
                                setState(() {
                                  this.isLoading = true;
                                });
                                checkPermissions();
//                          this.checkPermissions();
                              },
                            )
                          : Center(
                              child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  TratoColors.orange),
                            )),
                    ),
                  )
                ])),
            body: SingleChildScrollView(
                child: MainCustomCard(
                    icon: ImageConstants.fingerprint,
                    title: StringConstants.fingerprint_title,
                    content: StringConstants.fingerprint_content))));
  }

  Future<void> checkPermissions() async {
    if (Platform.isAndroid) {
      PermissionStatus permissionExternalStorageResult =
          await SimplePermissions.requestPermission(
              Permission.WriteExternalStorage);
      PermissionStatus permissionPhotoLibraryResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionExternalStorageResult == PermissionStatus.authorized &&
          permissionPhotoLibraryResult == PermissionStatus.authorized) {
        this._getFingerprint();
      } else {
        this.showAlertPermissions();
      }
    } else {
      PermissionStatus permissionPhotoLibraryResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionPhotoLibraryResult == PermissionStatus.authorized) {
        this._getFingerprint();
      } else {
        showAlertPermissions();
      }
    }
  }

  void showAlertPermissions() {
    showDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
              title: Text(StringConstants.alert),
              content: Text(StringConstants.alertPermissionsQR),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("Dar permisos"),
                  isDefaultAction: true,
                  onPressed: () {
                    ExtendedNavigator.rootNavigator.pop();
                    if (Platform.isIOS) {
                      AppSettings.openAppSettings();
                    } else {
                      this.checkPermissions();
                    }
                  },
                ),
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text("Cancelar"),
                  onPressed: () {
                    setState(() {
                      isLoading = false;
                    });
                    ExtendedNavigator.rootNavigator.pop();
                  },
                )
              ],
            ),
        barrierDismissible: true);
  }

  Future<void> _getFingerprint() async {
    try {
      final String result =
          await platform.invokeMethod(StringConstants.getFingerId);
      ExtendedNavigator.rootNavigator.pushNamed(Routes.uploadFingerprintScreen,
          arguments: UploadFingerprintScreenArguments(
              contractModel: widget.contractModel,
              fingerprintResponse: result));
//      FingerprintManager()
//          .uploadFingerprints(_subject, result, widget.contractModel.sId);
//      this.uploadFingerPrint(result);
    } on PlatformException catch (exception) {
      this.setState(() {
        this.isLoading = false;
      });
      Navigation.showToast('Intenta de nuevo.', context);
    }
  }
}
