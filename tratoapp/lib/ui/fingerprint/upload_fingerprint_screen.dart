import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/fingerprint_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class UploadFingerprintScreen extends StatefulWidget {
  const UploadFingerprintScreen({this.contractModel, this.fingerprintResponse});

  final ContractModel contractModel;
  final String fingerprintResponse;

  @override
  _UploadFingerprintState createState() => _UploadFingerprintState();
}

class _UploadFingerprintState extends State<UploadFingerprintScreen>
    with AfterLayoutMixin {
  bool isUploadingFingerprints = false;

  final BehaviorSubject<ApiResponse> _subjectUploadFingerprints =
      BehaviorSubject<ApiResponse>();

  @override
  void dispose() {
    super.dispose();
    _subjectUploadFingerprints.close();
  }

  @override
  void initState() {
    super.initState();
    this.setState(() {
      this.isUploadingFingerprints = true;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    print('fingerprints: ${widget.fingerprintResponse}');
    FingerprintManager().uploadFingerprints(_subjectUploadFingerprints,
        widget.fingerprintResponse, widget.contractModel.sId);
//    this.uploadFingerPrint(result);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: SingleChildScrollView(
              child: isUploadingFingerprints
                  ? MainCustomCard(
                      icon: ImageConstants.clockIcon,
                      title: StringConstants.title_uploading_fingerprint,
                      content: StringConstants.content_uploading_fingerprint,
                      isLoading: true,
                    )
                  : MainCustomCard(
                      icon: ImageConstants.mailIcon,
                      title: StringConstants.title_success_uploading_video,
                      content: StringConstants
                          .content_success_uploading_fingerprints,
                      isLoading: false,
                    )),
          bottomNavigationBar: !isUploadingFingerprints
              ? SafeArea(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                      Card(
                        elevation: 20.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0),
                        )),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 30, right: 30, top: 20, bottom: 40),
                          child: NiceButton(
                            elevation: 8,
                            radius: 8,
                            background: TratoColors.orange,
                            text: "Continuar",
                            fontSize: 18,
                            onPressed: () {
//                            if (widget
//                                .contractModel.shield3ValidationRequired) {
//                              ExtendedNavigator.rootNavigator.pushNamed(
//                                  Routes.waitingValidationScreen,
//                                  arguments: WaitingValidationScreenArguments(
//                                      contractModel: widget.contractModel));
//                            } else {
                              ExtendedNavigator.rootNavigator.pushNamed(
                                  Routes.signatureScreen,
                                  arguments: SignatureScreenArguments(
                                      contractModel: widget.contractModel));
//                            }
                            },
                          ),
                        ),
                      )
                    ]))
              : Empty(),
        ));
  }
}
