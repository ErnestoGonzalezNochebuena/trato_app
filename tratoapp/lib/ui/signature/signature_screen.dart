import 'dart:async';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:app_settings/app_settings.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:rxdart/rxdart.dart';
import 'package:signature/signature.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/manager/get_contract_manager.dart';
import 'package:tratoapp/manager/signature_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/coord_model/coord_model.dart';
import 'package:tratoapp/model/location_model/location_model.dart';
import 'package:tratoapp/model/signature_model/signature_model.dart';
import 'package:tratoapp/model/signature_request/signature_request.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';

class SignatureScreen extends StatefulWidget {
  final ContractModel contractModel;

  const SignatureScreen({@required this.contractModel});

  @override
  _SignatureState createState() => _SignatureState();
}

class _SignatureState extends State<SignatureScreen>
    with AfterLayoutMixin<SignatureScreen> {
  final key = new GlobalKey();

  final BehaviorSubject<SignatureModel> _subjectUploadSignature =
      BehaviorSubject<SignatureModel>();
  final BehaviorSubject<ApiResponse> _subjectGetContract =
      BehaviorSubject<ApiResponse>();

  final SignatureController _controller = SignatureController(
    penStrokeWidth: 3,
    penColor: Colors.black,
  );

  bool loading = false;

  @override
  void dispose() {
    super.dispose();
    _subjectUploadSignature.close();
    _subjectGetContract.close();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _controller.addListener(() {
      print("Value changed");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        )),
        leading: IconButton(
          iconSize: 40,
          icon: Icon(CupertinoIcons.back),
          color: Colors.black54,
          onPressed: () {
            ExtendedNavigator.rootNavigator.pop();
          },
        ),
        backgroundColor: Colors.white,
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
              child: Image.asset(
                ImageConstants.mainLogo,
                height: 30.0,
                width: 120.0,
              ))
        ],
      ),
      body: SingleChildScrollView(
          child: Padding(
              padding:
                  EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 20),
              child: Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  )),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                            padding:
                                EdgeInsets.only(top: 20.0, right: 10, left: 10),
                            child: CustomText(
                                customText: StringConstants.title_signature,
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                textColor: Colors.black54)),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: CustomText(
                                customText: StringConstants.content_signature,
                                fontSize: 15,
                                align: TextAlign.justify,
                                textColor: Colors.black54)),
                        AbsorbPointer(
                            absorbing: loading,
                            child: Signature(
                              key: key,
                              controller: _controller,
                              height: 300,
                              backgroundColor: Colors.white,
                            )),
                        AbsorbPointer(
                            absorbing: loading,
                            child: Align(
                                alignment: Alignment.bottomCenter,
                                child: NiceButton(
                                  elevation: 8,
                                  radius: 8,
                                  background: TratoColors.orange,
                                  text: "Reiniciar",
                                  fontSize: 18,
                                  onPressed: () {
                                    _controller.clear();
                                  },
                                ))),
                        SizedBox(
                          height: 10.0,
                        )
                      ])))),
      bottomNavigationBar: SafeArea(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
            Card(
                elevation: 20.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                )),
                child: Padding(
                  padding:
                      EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 40),
                  child: !loading
                      ? NiceButton(
                          elevation: 8,
                          radius: 8,
                          background: TratoColors.orange,
                          text: "Firmar",
                          fontSize: 18,
                          onPressed: () {
                            if (_controller.isNotEmpty) {
                              this.setState(() {
                                this.loading = true;
                              });
                              if (widget.contractModel.statuses.participant
                                  .requestLocation) {
                                this.checkPermissions();
                              } else {
                                this.generateSignatureModel();
                              }
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (_) => CupertinoAlertDialog(
                                        title: Text(StringConstants.alert),
                                        content: Text(StringConstants
                                            .alertEmptySignatures),
                                        actions: <Widget>[
                                          CupertinoDialogAction(
                                            isDefaultAction: true,
                                            child: Text("OK"),
                                            onPressed: () {
                                              ExtendedNavigator.rootNavigator
                                                  .pop();
                                            },
                                          )
                                        ],
                                      ),
                                  barrierDismissible: true);
                            }
                          },
                        )
                      : Center(
                          child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              TratoColors.orange),
                        )),
                ))
          ])),
    );
  }

  void uploadSignature(SignatureModel model, Position position) {
    LocationModel locationModel;
    if (position == null) {
      locationModel = LocationModel(longitude: 0, latitude: 0);
    } else {
      locationModel = LocationModel(
          longitude: position.longitude, latitude: position.latitude);
    }
    SignatureRepository().uploadSignature(
        _subjectUploadSignature,
        SignatureRequest(signatureCoords: model, geolocation: locationModel),
        widget.contractModel.sId,
        widget.contractModel.statuses.participant.sId);
    _subjectUploadSignature.stream.listen((apiResponse) {
      SignatureModel model = apiResponse;
      if (model.success) {
        GetContractManager()
            .show(_subjectGetContract, widget.contractModel.sId, {});
      } else {
        this.setState(() {
          this.loading = false;
        });
        Navigation.showToast('${model.error} Intenta de nuevo.', context);
      }
    });
    _subjectGetContract.stream.listen((apiResponse) {
      this.setState(() {
        this.loading = false;
      });
      if (apiResponse.model == null) {
        Navigation.showToast('No fue posible actualizar el contrato.', context);
        ExtendedNavigator.rootNavigator.pushNamed(Routes.successSignatureScreen,
            arguments: SuccessSignatureScreenArguments(
                contractModel: widget.contractModel));
      } else {
        ContractModel model = apiResponse.model;
        if (model.status == StringConstants.status021 ||
            model.status == StringConstants.status040) {
          model.shield3ValidationRequired = false;
          ExtendedNavigator.rootNavigator.pushNamed(
              Routes.successValidationScreen,
              arguments:
                  SuccessValidationScreenArguments(contractModel: model));
        } else {
          ExtendedNavigator.rootNavigator.pushNamed(
              Routes.successSignatureScreen,
              arguments: SuccessSignatureScreenArguments(
                  contractModel: widget.contractModel));
        }
      }
    });
  }

  generateSignatureModel() {
    _controller.toPngBytes().then((value) {
      _controller.toImage().then((image) async {
//        model.date = DateTime.now().millisecondsSinceEpoch;
//        model.height = image.height.toDouble();
//        model.width = image.width.toDouble();
        Position position;
        if (widget.contractModel.statuses.participant.requestLocation) {
          position = await Geolocator()
              .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        }
        List<List<CoordenateModel>> coords = List();
        List<CoordenateModel> points;
        _controller.points.forEach((e) {
          if (e.type == PointType.move) {
            if (points == null) {
              points = List();
            }
            points.add(CoordenateModel(
                x: e.offset.dx,
                y: e.offset.dy,
                time: DateTime.now().millisecondsSinceEpoch,
                color: "black"));
          } else {
            if (points != null) {
              coords.add(points);
              points = null;
            }
          }
        });
        final RenderBox renderBoxRed = key.currentContext.findRenderObject();
        final signatureSize = renderBoxRed.size;

//        model.coords = points;
        SignatureModel model = SignatureModel(
            date: DateTime.now().millisecondsSinceEpoch,
            height: signatureSize.height,
            width: signatureSize.width,
            signAsObject: true,
            coords: coords);
        this.uploadSignature(model, position);
      });
    });
  }

  Future<void> checkPermissions() async {
    // if (Platform.isAndroid) {
//      PermissionStatus permissionLocation =
//          await SimplePermissions.requestPermission(
//              Permission.AccessFineLocation);
    PermissionStatus permissionLocationCoarse =
        await SimplePermissions.requestPermission(
            Permission.AccessCoarseLocation);
    if (
//      permissionLocation == PermissionStatus.authorized &&
        permissionLocationCoarse == PermissionStatus.authorized) {
      generateSignatureModel();
    } else {
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertLocationPermissions),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Dar permisos"),
                    isDefaultAction: true,
                    onPressed: () {
                      this.setState(() {
                        this.loading = false;
                      });
                      ExtendedNavigator.rootNavigator.pop();
                      if (Platform.isIOS) {
                        AppSettings.openLocationSettings();
                      } else {
                        this.checkPermissions();
                      }
                    },
                  ),
                  CupertinoDialogAction(
                    isDefaultAction: true,
                    child: Text("Cancelar"),
                    onPressed: () {
                      this.setState(() {
                        this.loading = false;
                      });
                      ExtendedNavigator.rootNavigator.pop();
                    },
                  )
                ],
              ),
          barrierDismissible: true);
    }
    // } else {
    //   this.checkPermissions();
  }
// }
}
