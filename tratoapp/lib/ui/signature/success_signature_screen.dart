import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:open_file/open_file.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/show_contract_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/pdf_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/services/pdf_service_provider.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class SuccessSignatureScreen extends StatefulWidget {
  final ContractModel contractModel;

  const SuccessSignatureScreen({@required this.contractModel});

  @override
  _SuccessSignatureState createState() => _SuccessSignatureState();
}

class _SuccessSignatureState extends State<SuccessSignatureScreen>
    with AfterLayoutMixin<SuccessSignatureScreen> {
  String urlPDFPath;
  bool _loading = false;
  String localPath;

  String _openResult = 'Unknown';

  Future<void> openFile() async {
    final result = await OpenFile.open(localPath);

    setState(() {
      _openResult = "type=${result.type}  message=${result.message}";
    });
  }

  Future<void> _getPdf() async {
    ApiServiceProvider.loadPDF(urlPDFPath,false).then((value) {
      setState(() {
        localPath = value;
        this._loading = false;
        openFile();
      });
    });

    this.setState(() {
      this._loading = true;
    });
  }

  final BehaviorSubject<ApiResponse> _subjectContractScreen =
      BehaviorSubject<ApiResponse>();

  @override
  void afterFirstLayout(BuildContext context) {
    ShowContractManager()
        .show(_subjectContractScreen, widget.contractModel.sId, {});
    _subjectContractScreen.listen((apiResponse) {
      if (apiResponse.model != null) {
        PdfModel model = apiResponse.model;
        urlPDFPath = model.url;
        _initPdf();
      }
    });
  }

  Future<void> _initPdf() async {
    ApiServiceProvider.loadPDF(urlPDFPath,false).then((value) {
      setState(() {
        localPath = value;
        this._loading = false;
      });
    });

    this.setState(() {
      this._loading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: SingleChildScrollView(
              child: MainCustomCard(
            icon: ImageConstants.checkIcon,
            title: StringConstants.title_success_signature,
            content: StringConstants.content_success_signature,
            isLoading: _loading,
          )),
          bottomNavigationBar: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                Card(
                  elevation: 20.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  )),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 30, right: 30, top: 20, bottom: 40),
                    child: NiceButton(
                      elevation: 8,
                      radius: 8,
                      background: TratoColors.orange,
                      text:
                          // widget.contractModel.shield3ValidationRequired
                          // ? StringConstants.downloadContract
                          // :
                          StringConstants.next,
                      fontSize: 18,
                      onPressed: () {
                        // widget.contractModel.shield3ValidationRequired
                        // ? _getPdf()
                        // :
//                        if (widget.contractModel.statuses.participant.status ==
//                            StringConstants.status037) {
                        ExtendedNavigator.rootNavigator
                              .pushNamedAndRemoveUntil(
                                  Routes.contractListScreen, (route) => false,
                                  arguments: ContractListScreenArguments(
                                      isBackFromContract: true));
//                        } else {
//                          ExtendedNavigator.rootNavigator.pushNamed(
//                              Routes.waitingValidationScreen,
//                              arguments: WaitingValidationScreenArguments(
//                                  contractModel: widget.contractModel));
//                        }
                      },
                    ),
                  ),
                )
              ])),
        ));
  }
}
