import 'dart:async';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:app_settings/app_settings.dart';
import 'package:auto_route/auto_route.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_pusher/pusher.dart';
import 'package:nice_button/nice_button.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:rxdart/rxdart.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/get_contract_manager.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/contracts_indicator_model.dart';
import 'package:tratoapp/model/session_jwt.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/ui/splash/splash_screen.dart';
import 'package:tratoapp/utils/application_settings.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';
import 'package:tratoapp/utils/utilities.dart';
import 'package:uni_links/uni_links.dart';

class ContractListScreen extends StatefulWidget {
  final bool isBackFromContract;

  ContractListScreen({this.isBackFromContract = false});

  @override
  _ContractListState createState() => _ContractListState();
}

class _ContractListState extends State<ContractListScreen>
    with AfterLayoutMixin {
  List<ContractModel> contracts = List();
  bool _isLoading = true;
  final BehaviorSubject<ApiResponse> _subjectSessionJwt =
      BehaviorSubject<ApiResponse>();
  final BehaviorSubject<ApiResponse> _subjectContract =
      BehaviorSubject<ApiResponse>();
  final BehaviorSubject<ApiResponse> _subjectLoadContracts =
      BehaviorSubject<ApiResponse>();

  String _latestLink = 'Unknown';
  Uri _latestUri;
  UniLinksType _type = UniLinksType.string;
  StreamSubscription _sub;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  double currentStep = 0;

  bool isAlertShowed = false;

  @override
  void dispose() {
    super.dispose();
    _subjectContract.close();
    _subjectSessionJwt.close();
    _subjectLoadContracts.close();
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      this.initPlatformState();
    }else {
      this.initDynamicLinks();
    }
    // this._onRefresh();
    _subjectLoadContracts.listen((value) {
      if (value.models != null) {
        this.setState(() {
          this._isLoading = false;
          this.contracts = value.models;
          this._refreshController.refreshCompleted();
        });
      } else {
        if (value.model != null) {
          IndicatorsModel model = value.model as IndicatorsModel;
          while (this.currentStep < model.currentStep) {
            setState(() {
              this.currentStep = this.currentStep + 1;
            });
          }
        }
      }
    });
  }

  void _fetchContracts() {
    GetContractManager().loadContracts(_subjectLoadContracts);
  }

  void _onRefresh() {
    this.setState(() {
      this._isLoading = true;
    });
    this.currentStep = 0;
    this._fetchContracts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        )),
        backgroundColor: Colors.white,
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
              child: Image.asset(
                ImageConstants.mainLogo,
                height: 30.0,
                width: 120.0,
              ))
        ],
      ),
      body: !_isLoading
          ? contracts.length > 0
              ? SmartRefresher(
                  enablePullDown: true,
                  header: MaterialClassicHeader(
                    backgroundColor: TratoColors.orange,
                    color: Colors.white,
                  ),
                  controller: _refreshController,
                  onRefresh: () {
                    _onRefresh();
                  },
                  child: Column(mainAxisSize: MainAxisSize.max, children: [
                    Flexible(
                        child: SmartRefresher(
                            enablePullDown: true,
                            header: MaterialClassicHeader(
                              backgroundColor: TratoColors.orange,
                              color: Colors.white,
                            ),
                            controller: _refreshController,
                            onRefresh: () => _onRefresh(),
                            child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: this.contracts.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return ContractItem(
                                      onUpdateContracts: () {
                                        this.setState(() {
                                          this._isLoading = true;
                                          this._fetchContracts();
                                        });
                                      },
                                      contractModel:
                                          this.contracts.elementAt(index));
                                }))),
                  ]),
                )
              : SingleChildScrollView(
                  child: MainCustomCard(
                      icon: "",
                      title: StringConstants.title_no_contracts,
                      content: StringConstants.content_no_contracts,
                      isLoading: false,
                      footer: NiceButton(
                        elevation: 8,
                        radius: 8,
                        background: TratoColors.orange,
                        text: StringConstants.scan_qr,
                        fontSize: 16,
                        onPressed: () =>
                            // onPressed: () =>
                            // this.launchExternalLink(
                            //     "https://enterprise.app.trato.io/sign/5f5b84206e42ef0ae44b90c6/1b8d9460-aed4-46b3-b9f5-6c8fdcea7663-b64906d3-e506-42d3-8652-847ae8170955"),
                            this.scanQr(),
                      )))
          : Center(
              child: Container(
                  width: 200,
                  child: Image.asset(
                    ImageConstants.tratoAnimation,
                    // height: 125.0,
                    // width: 125.0,
                  ))),
      bottomNavigationBar: !_isLoading
          ? contracts.length > 0
              ? SafeArea(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                      Card(
                          elevation: 20.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0),
                          )),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 30, right: 30, top: 20, bottom: 40),
                              child: NiceButton(
                                  elevation: 8,
                                  radius: 8,
                                  background: TratoColors.orange,
                                  text: StringConstants.scan_qr,
                                  fontSize: 15,
                                  onPressed: () {
                                    this.scanQr();
                                    // this.launchExternalLink(
                                    //     "https://enterprise-test.app.trato.io/sign/5f4eaa6d6c0c7f3d67d40017/f03fc205-5cbb-4b5c-a6c8-3ed71cc72470-02d4015c-e5e1-40e3-993f-77c8ea5d2136?lang=ES");
                                  })))
                    ]))
              : Empty()
          : Empty(),
    );
  }

  scanQr() async {
    if (Platform.isAndroid) {
      PermissionStatus permissionExternalStorageResult =
          await SimplePermissions.requestPermission(
              Permission.WriteExternalStorage);
      PermissionStatus permissionPhotoLibraryResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionExternalStorageResult == PermissionStatus.authorized &&
          permissionPhotoLibraryResult == PermissionStatus.authorized) {
        this.getScanResults().then((value) => launchExternalLink(value));
      } else {
        this.showAlertPermissions();
      }
    } else {
      PermissionStatus permissionPhotoLibraryResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionPhotoLibraryResult == PermissionStatus.authorized) {
        this.getScanResults().then((value) => launchExternalLink(value));
      } else {
        showAlertPermissions();
      }
    }
  }

  void showAlertPermissions() {
    showDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
              title: Text(StringConstants.alert),
              content: Text(StringConstants.alertPermissionsQR),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("Dar permisos"),
                  isDefaultAction: true,
                  onPressed: () {
                    ExtendedNavigator.rootNavigator.pop();
                    if (Platform.isIOS) {
                      AppSettings.openAppSettings();
                    } else {
                      this.scanQr();
                    }
                  },
                ),
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text("Cancelar"),
                  onPressed: () {
                    ExtendedNavigator.rootNavigator.pop();
                  },
                )
              ],
            ),
        barrierDismissible: true);
  }

  void initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        this.launchExternalLink(deepLink.toString());
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  initPlatformState() async {
    if (_type == UniLinksType.string) {
      await initPlatformStateForString();
    } else {
      await initPlatformStateForUri();
    }
  }

  // URL init from schema
  initPlatformStateForString() async {
    String initialLink;
    Uri initialUri;

    //Deeplink match
    _sub = getLinksStream().listen((String link) {
      print("!!!!!!!!!!!! getLinksStream().listen " + link);
      if (!mounted) return;
      setState(() {
        _latestLink = link ?? 'Unknown';
        _latestUri = null;
        try {
          if (link != null) _latestUri = Uri.parse(link);
        } on FormatException {}
      });
    }, onError: (err) {
      print("!!!!!!!!!!!! getLinksStream().listen.error " + err);
      if (!mounted) return;
      setState(() {
        _latestLink = 'Failed to get latest link: $err.';
        _latestUri = null;
      });
    });

    // Attach a second listener to the stream
    getLinksStream().listen((String link) {
      print('got link: $link');
      if (!mounted) return;
      this.setState(() {
        if (link != null) {
          this.launchExternalLink(link);
          link = null;
        }
      });
    }, onError: (err) {
      print('got err: $err');
    });

    // PlatformException.
    try {
      initialLink = await getInitialLink();
      print('initial link: $initialLink');
      if (initialLink != null) initialUri = Uri.parse(initialLink);
      this.setState(() {
        if (initialLink != null) {
          if (!widget.isBackFromContract) {
            this.launchExternalLink(initialLink);
            initialLink = null;
          }
        }
      });
    } on PlatformException {
      initialLink = 'Failed to get initial link.';
      initialUri = null;
    } on FormatException {
      initialLink = 'Failed to parse the initial link as Uri.';
      initialUri = null;
    }

    //set final state
    if (!mounted) return;

    setState(() {
      _latestLink = initialLink;
      _latestUri = initialUri;
    });
  }

  // URI
  initPlatformStateForUri() async {
    Uri initialUri;
    String initialLink;

    _sub = getUriLinksStream().listen((Uri uri) {
      if (!mounted) return;
      setState(() {
        _latestUri = uri;
        _latestLink = uri?.toString() ?? 'Unknown';
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        _latestLink = 'Failed to get latest link: $err.';
      });
    });

    // Attach a second listener to the stream
    getUriLinksStream().listen((Uri uri) {
      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
    }, onError: (err) {
      print('got err: $err');
    });

    // PlatformException.
    try {
      initialUri = await getInitialUri();
      print('initial uri: ${initialUri?.path}'
          ' ${initialUri?.queryParametersAll}');
      initialLink = initialUri?.toString();
    } on PlatformException {
      initialUri = null;
      initialLink = 'Failed to get initial uri.';
    } on FormatException {
      initialUri = null;
      initialLink = 'Bad parse the initial link as Uri.';
    }

    //set final state
    if (!mounted) return;

    setState(() {
      _latestUri = initialUri;
      _latestLink = initialLink;
    });
  }

  void launchExternalLink(String link) {
    setState(() {
      this._isLoading = true;
    });
    print(link);

    SessionManager().getSessionByLink(_subjectSessionJwt, link);
    print(link);
    this._subjectSessionJwt.stream.listen((apiResponse) async {
      if (apiResponse.model != null) {
        SessionJwt session = apiResponse.model;
        if (session.success) {
          await SessionManager().setSessionToken(session.jwt);
          this.getContract(link);
        } else {
          this.setState(() {
            this._isLoading = false;
          });
          Navigation.showToast(StringConstants.contract_no_available, context);
        }
      }
    });
  }

  void getContract(String link) {
    String contractId = Utilities().getContractId(link);
    GetContractManager().show(_subjectContract, contractId, {});
    this._subjectContract.stream.listen((apiResponse) {
      this.setState(() {
        _isLoading = false;
      });
      if (apiResponse.model != null) {
        ContractModel model = apiResponse.model;
        if (model.message == StringConstants.status047) {
          this.showAlertContractCancelled();
          GetContractManager().removeContract(link);
        } else if (model.status == "") {
          this.showAlertNotReadyToSign();
          GetContractManager().removeContract(link);
          // GetContractManager().loadContracts(_subjectLoadContracts);
        } else if (model.message == StringConstants.status050) {
          this.showAlertNotReadyToSign();
          GetContractManager().removeContract(link);
          // GetContractManager().loadContracts(_subjectLoadContracts);
        } else if (model.status == StringConstants.status049 ||
            model.statuses.participant == null) {
          this.showAlertNoParticipantContract();
          GetContractManager().removeContract(link);
          // GetContractManager().loadContracts(_subjectLoadContracts);
        } else {
          GetContractManager().addContract(link);
          this._navigate(model, link);
        }
        // }
      } else {
        Navigation.showToast(StringConstants.contract_no_available, context);
      }
    });
  }

  void _navigate(ContractModel contractModel, String contractLink) {
    // if (contractModel.status == null) {
    /**Usuario ya no es parte del contrato */
    if (contractModel.message == StringConstants.status049 ||
        contractModel.statuses.participant == null) {
      this.showAlertNoParticipantContract();
      GetContractManager().removeContract(contractLink);
      // GetContractManager().loadContracts(_subjectLoadContracts);
    } else if (contractModel.status == StringConstants.status013 ||
        contractModel.status == StringConstants.status028 ||
        contractModel.status == StringConstants.status015) {
      this.showAlertNoAvailableContract();
      GetContractManager().removeContract(contractLink);
      // GetContractManager().loadContracts(_subjectLoadContracts);
    } else if (contractModel.message == StringConstants.status048) {
      /**Pendiente de aceptar términos y condiciones**/
      ExtendedNavigator.rootNavigator.pushNamed(Routes.acceptConditions,
          arguments: AcceptConditionsArguments(contractModel: contractModel));
    } else if (contractModel.status == StringConstants.status021 ||
        contractModel.status == StringConstants.status040 ||
        /**Blindaje 1**/
        contractModel.status == StringConstants.status038 ||
        contractModel.status == StringConstants.status027) {
      contractModel.shield3ValidationRequired = false;
      ExtendedNavigator.rootNavigator.pushNamed(Routes.successValidationScreen,
          arguments:
              SuccessValidationScreenArguments(contractModel: contractModel));
      // } else if (contractModel.status == StringConstants.status013 ||
      //     contractModel.status == StringConstants.status028 ||
      //     contractModel.status == StringConstants.status015) {
      //   this.showAlertNoAvailableContract();
      //   GetContractManager().removeContract(contractLink);

    } else {
      /**Validaciones de bliindaje 3**/
      if (contractModel.statuses.participant.requestAgreement) {
        /**Usuario NO envio video de validación**/
        if (contractModel.statuses.participant.video == null) {
          ExtendedNavigator.rootNavigator.pushNamed(Routes.videoCaptureScreen,
              arguments:
                  VideoCaptureScreenArguments(contractModel: contractModel));
        } else {
          /**Contrato sin validación de video**/
          if (!contractModel.shield3ValidationRequired) {
            /**Usuario ya subio video de validación y  SI firmo el contrato **/
            if (contractModel.statuses.participant.status ==
                StringConstants.status037) {
              ExtendedNavigator.rootNavigator.pushNamed(
                  Routes.successSignatureScreen,
                  arguments: SuccessSignatureScreenArguments(
                      contractModel: contractModel));
              /**Usuario ya subio video de validación y NO firmo el contrato **/
            } else {
              ExtendedNavigator.rootNavigator.pushNamed(Routes.signatureScreen,
                  arguments:
                      SignatureScreenArguments(contractModel: contractModel));
            }
          } else {
            /**Usuario ya subio video de validación y video SI ha sido validado **/
            if (contractModel.statuses.participant.videoValidated) {
              /**El video SI ha sido validado y el usuario SI ha firmado**/
              if (contractModel.statuses.participant.status ==
                  StringConstants.status037) {
                ExtendedNavigator.rootNavigator.pushNamed(
                    Routes.successSignatureScreen,
                    arguments: SuccessSignatureScreenArguments(
                        contractModel: contractModel));
              } else {
                /**El video SI ha sido validado y el usuario NO ha firmado**/
                ExtendedNavigator.rootNavigator.pushNamed(
                    Routes.signatureScreen,
                    arguments:
                        SignatureScreenArguments(contractModel: contractModel));
              }
            } else {
              /**El video NO ha sido validado y el usuario NO ha firmado**/
              ExtendedNavigator.rootNavigator.pushNamed(
                  Routes.waitingValidationScreen,
                  arguments: WaitingValidationScreenArguments(
                      contractModel: contractModel));
            }
          }
        }
      } else if (contractModel.statuses.participant.shield4DataRequested) {
        /**Validaciones de blindaje 4**/
        ExtendedNavigator.rootNavigator.pushNamed(Routes.contractScreen,
            arguments: ContractScreenArguments(
                contractModel: contractModel, isSignPreview: false));
//        ExtendedNavigator.rootNavigator.pushNamed(Routes.fingerprintScreen,
//            arguments:
//                FingerprintScreenArguments(contractModel: contractModel));
      } else {
        /**Validaciones de blindaje 2**/
        /**El usuario SI ha firmado**/
        if (contractModel.signatureType == StringConstants.autograph) {
          if (contractModel.statuses.participant.status ==
              StringConstants.status037) {
            ExtendedNavigator.rootNavigator.pushNamed(
                Routes.successSignatureScreen,
                arguments: SuccessSignatureScreenArguments(
                    contractModel: contractModel));
          } else {
            /**El usuario NO ha firmado**/
            ExtendedNavigator.rootNavigator.pushNamed(Routes.contractScreen,
                arguments: ContractScreenArguments(
                    contractModel: contractModel, isSignPreview: false));
          }
        } else {
//          Navigation.showToast(StringConstants.contract_no_available, context);
          this.showAlertNoAvailableContract();
          GetContractManager().removeContract(contractLink);
          // GetContractManager().loadContracts(_subjectLoadContracts);
        }
      }
    }
  }

  Future<String> getScanResults() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "Cancelar",
          "flash_on": "Encender flash",
          "flash_off": "Apagar flash",
        },
      );
      ScanResult barcode = await BarcodeScanner.scan(options: options);
      print(barcode);
      return barcode.rawContent;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          print('El usuario no dio permiso para el uso de la cámara!');
          return "";
        });
      } else {
        print('Error desconocido $e');
        return "";
      }
    } on FormatException {
      print(
          'nulo, el usuario presionó el botón de volver antes de escanear algo)');
      return "";
    } catch (e) {
      print('Error desconocido : $e');
      return "";
    }
  }

  void showAlertNoAvailableContract() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertContractNoAvailable),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }

  void showAlertNoParticipantContract() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertYouAreNotParticipant),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }

  void showAlertContractCancelled() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertCanceledContract),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }

  void showAlertNotReadyToSign() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertContractNotReady),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    this._onRefresh();
  }
}

class ContractItem extends StatefulWidget {
  final ContractModel contractModel;
  final VoidCallback onUpdateContracts;

  const ContractItem(
      {@required this.contractModel, @required this.onUpdateContracts});

  @override
  _ContractState createState() => _ContractState();
}

class _ContractState extends State<ContractItem> {
  Channel _channel;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  bool isAlertShowed = false;

  @override
  void initState() {
    super.initState();
    this._initPusher();
    this.initializing();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            )),
            child: InkWell(
                onTap: () {
                  this._navigate(widget.contractModel);
                  SessionManager().setSessionToken(widget.contractModel.jwt);
                },
                child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(width: 10.0),
                        widget.contractModel?.user != null
                            ? Image.network(
                                widget.contractModel.user.office.logo,
                                width: 50,
                                height: 50,
                              )
                            : Image.asset(
                                ImageConstants.documentsLogo,
                                width: 50,
                                height: 50,
                              ),
                        SizedBox(width: 10.0),
                        Flexible(
                            child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              fontSize: 16.0,
                              align: TextAlign.start,
                              maxLines: 10,
                              fontWeight: FontWeight.bold,
                              customText: _getContractTitle(),
                            ),
                            CustomText(
                              fontSize: 13.0,
                              align: TextAlign.start,
                              maxLines: 5,
                              customText: _getStatusMessage(),
                            )
                          ],
                        ))
                      ],
                    ))),
          )),
    ]);
  }

  _getContractTitle() {
    if (widget.contractModel.documentId != null) {
      return "${widget.contractModel.documentId}-${widget.contractModel?.name}";
    } else {
      return widget.contractModel?.name;
    }
  }

  _getStatusMessage() {
    if (widget.contractModel.message != null) {
      return Utilities.getStatus(widget.contractModel.message);
    } else {
      if (widget.contractModel.status != null) {
        if (widget.contractModel.status == StringConstants.status021) {
          return StringConstants.message021;
        } else {
          if (widget.contractModel.statuses.participant == null ||
              widget.contractModel.message == StringConstants.status049) {
            return Utilities.getStatus("No eress parte del contrato");
          } else {
            return Utilities.getStatus(
                widget.contractModel.statuses.participant.status);
          }
        }
      } else {
        return 'Actualizando...';
      }
    }
    // return widget.contractModel.message != null
    //     ? Utilities.getStatus(widget.contractModel.message)
    //     : widget.contractModel.status != null
    //         ? widget.contractModel.status == StringConstants.status021
    //             ? StringConstants.message021
    //             : Utilities.getStatus(
    //                 widget.contractModel.statuses.participant.status)
    //         : 'Cargando...';
  }

  void showAlertNoParticipantContract() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertYouAreNotParticipant),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }

  void _navigate(ContractModel contractModel) {
    // if (contractModel.status == null) {
    /**Usuario ya no es parte del contrato */
    if (contractModel.message == StringConstants.status049 ||
        contractModel.statuses.participant == null) {
      this.showAlertNoParticipantContract();
    } else if (contractModel.status == StringConstants.status013 ||
        contractModel.status == StringConstants.status028 ||
        contractModel.status == StringConstants.status015) {
      this.showAlertNoAvailableContract();
      // GetContractManager().removeContract(contractLink);
    } else if (contractModel.message == StringConstants.status048) {
      /**Pendiente de aceptar términos y condiciones**/
      ExtendedNavigator.rootNavigator.pushNamed(Routes.acceptConditions,
          arguments: AcceptConditionsArguments(contractModel: contractModel));
    } else if (contractModel.status == StringConstants.status021 ||
        contractModel.status == StringConstants.status040 ||
        /**Blindaje 1**/
        contractModel.status == StringConstants.status038 ||
        contractModel.status == StringConstants.status027) {
      contractModel.shield3ValidationRequired = false;
      ExtendedNavigator.rootNavigator.pushNamed(Routes.successValidationScreen,
          arguments:
              SuccessValidationScreenArguments(contractModel: contractModel));
      // } else if (contractModel.status == StringConstants.status013 ||
      //     contractModel.status == StringConstants.status028 ||
      //     contractModel.status == StringConstants.status015) {
      //   this.showAlertNoAvailableContract();
      //   GetContractManager().removeContract(contractLink);

    } else {
      /**Validaciones de bliindaje 3**/
      if (contractModel.statuses.participant.requestAgreement) {
        /**Usuario NO envio video de validación**/
        if (contractModel.statuses.participant.video == null) {
          ExtendedNavigator.rootNavigator.pushNamed(Routes.videoCaptureScreen,
              arguments:
                  VideoCaptureScreenArguments(contractModel: contractModel));
        } else {
          /**Contrato sin validación de video**/
          if (!contractModel.shield3ValidationRequired) {
            /**Usuario ya subio video de validación y  SI firmo el contrato **/
            if (contractModel.statuses.participant.status ==
                StringConstants.status037) {
              ExtendedNavigator.rootNavigator.pushNamed(
                  Routes.successSignatureScreen,
                  arguments: SuccessSignatureScreenArguments(
                      contractModel: contractModel));
              /**Usuario ya subio video de validación y NO firmo el contrato **/
            } else {
              ExtendedNavigator.rootNavigator.pushNamed(Routes.signatureScreen,
                  arguments:
                      SignatureScreenArguments(contractModel: contractModel));
            }
          } else {
            /**Usuario ya subio video de validación y video SI ha sido validado **/
            if (contractModel.statuses.participant.videoValidated) {
              /**El video SI ha sido validado y el usuario SI ha firmado**/
              if (contractModel.statuses.participant.status ==
                  StringConstants.status037) {
                ExtendedNavigator.rootNavigator.pushNamed(
                    Routes.successSignatureScreen,
                    arguments: SuccessSignatureScreenArguments(
                        contractModel: contractModel));
              } else {
                /**El video SI ha sido validado y el usuario NO ha firmado**/
                ExtendedNavigator.rootNavigator.pushNamed(
                    Routes.signatureScreen,
                    arguments:
                        SignatureScreenArguments(contractModel: contractModel));
              }
            } else {
              /**El video NO ha sido validado y el usuario NO ha firmado**/
              ExtendedNavigator.rootNavigator.pushNamed(
                  Routes.waitingValidationScreen,
                  arguments: WaitingValidationScreenArguments(
                      contractModel: contractModel));
            }
          }
        }
      } else if (contractModel.statuses.participant.shield4DataRequested) {
        /**Validaciones de blindaje 4**/
        ExtendedNavigator.rootNavigator.pushNamed(Routes.contractScreen,
            arguments: ContractScreenArguments(
                contractModel: contractModel, isSignPreview: false));
//        ExtendedNavigator.rootNavigator.pushNamed(Routes.fingerprintScreen,
//            arguments:
//                FingerprintScreenArguments(contractModel: contractModel));
      } else {
        /**Validaciones de blindaje 2**/
        /**El usuario SI ha firmado**/
        if (contractModel.signatureType == StringConstants.autograph) {
          if (contractModel.statuses.participant.status ==
              StringConstants.status037) {
            ExtendedNavigator.rootNavigator.pushNamed(
                Routes.successSignatureScreen,
                arguments: SuccessSignatureScreenArguments(
                    contractModel: contractModel));
          } else {
            /**El usuario NO ha firmado**/
            ExtendedNavigator.rootNavigator.pushNamed(Routes.contractScreen,
                arguments: ContractScreenArguments(
                    contractModel: contractModel, isSignPreview: false));
          }
        } else {
//          Navigation.showToast(StringConstants.contract_no_available, context);
          this.showAlertNoAvailableContract();
          // GetContractManager().removeContract(contractLink);
        }
      }
    }
  }

  Future<void> _initPusher() async {
    SessionManager().hasSessionToken().then((hasToken) {
      if (hasToken) {
        SessionManager().getSessionToken().then((sessionJwt) async {
          try {
            await Pusher.init(
              ApplicationSettings.pusherApiKey,
              PusherOptions(
                  cluster: ApplicationSettings.pusherCluster,
                  auth: PusherAuth.fromJson({
                    'endpoint': ApplicationSettings.pusherEndpoint,
                    'headers': {'jwt': sessionJwt}
                  }),
                  encrypted: true),
            );
          } catch (err) {
            print(err);
          }

          Pusher.connect(onConnectionStateChange: (changeState) {
            print(changeState.currentState);
            if (changeState.currentState.trim().toLowerCase() ==
                ApplicationSettings.pusherConnected) {
              this.configureSocketChannel(widget.contractModel.sId);
            }
          }, onError: (error) {
            print(error.message);
          });
        });
      }
    });
  }

  void initializing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  void _showNotifications(String contractId) async {
    await notification(contractId);
  }

  Future onSelectNotification(String payLoad) {
    if (payLoad != null) {
      print(payLoad);
    }

    // we can set navigator to navigate another screen
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }

  Future<void> notification(String contractId) async {
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            'Channel ID', 'Channel title', 'channel body',
            priority: Priority.High,
            importance: Importance.Max,
            ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0,
        'Tu contrato ha sido actualizado',
        'Revisa el estatus de tu contrato.',
        notificationDetails);
  }

  Future<void> configureSocketChannel(String contractId) async {
//    var subscribedChannel = Pusher.getSocketId();
//    if (subscribedChannel != "${AppSettings.pusherChannelPrefix}$contractId") {
//    Pusher.unsubscribe("${AppSettings.pusherChannelPrefix}$contractId");
    Pusher.unsubscribe("${ApplicationSettings.pusherChannelPrefix}$contractId");
    _channel = await Pusher.subscribe(
        "${ApplicationSettings.pusherChannelPrefix}$contractId");
    _channel.bind(ApplicationSettings.pusherEventName, (onEvent) {
      print(onEvent.toString());
      widget.onUpdateContracts();
      _showNotifications(contractId);
    });
//    }
  }

  void showAlertNoAvailableContract() {
    if (this.isAlertShowed == false) {
      this.isAlertShowed = true;
      showDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(StringConstants.alert),
                content: Text(StringConstants.alertContractNoAvailable),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text("Aceptar"),
                    isDefaultAction: true,
                    onPressed: () {
                      Future.delayed(Duration(milliseconds: 500), () {
                        this.isAlertShowed = false;
                        ExtendedNavigator.rootNavigator.pop();
                      });
                    },
                  ),
                ],
              ),
          barrierDismissible: true);
    }
  }
}
