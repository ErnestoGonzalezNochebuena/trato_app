import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/strings.dart';

class PDFScreen extends StatefulWidget {
  final String path;

  PDFScreen({Key key, this.path}) : super(key: key);

  _PDFScreenState createState() => _PDFScreenState();
}

class _PDFScreenState extends State<PDFScreen> with WidgetsBindingObserver {
//  final Completer<PDFViewController> _controller =
//      Completer<PDFViewController>();
  int pages = 0;
  int currentPage = 0;
  bool isReady = false;
  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(StringConstants.documents),
          backgroundColor: TratoColors.orange,
          leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back),
          ),
        ),
        body: Stack(children: <Widget>[
//          _buildPDFView(),
//          _buildPDFView2(),
//          _buildButtonNavigation(),
        ]));
  }

//  _buildPDFView() {
//    return PDFView(
//      filePath: widget.path,
//      enableSwipe: true,
//      swipeHorizontal: false,
//      autoSpacing: false,
//      pageFling: true,
//      defaultPage: currentPage,
//      fitPolicy: FitPolicy.BOTH,
//      onRender: (_pages) {
//        setState(() {
//          pages = _pages;
//          isReady = true;
//        });
//      },
//      onError: (error) {
//        setState(() {
//          errorMessage = error.toString();
//        });
//        print(error.toString());
//      },
//      onPageError: (page, error) {
//        setState(() {
//          errorMessage = '$page: ${error.toString()}';
//        });
//        print('$page: ${error.toString()}');
//      },
//      onViewCreated: (PDFViewController pdfViewController) {
//        _controller.complete(pdfViewController);
//      },
//      onPageChanged: (int page, int total) {
//        print('page change: $page/$total');
//        setState(() {
//          currentPage = page;
//        });
//      },
//    );
//  }

//  _buildPDFView2() {
//    return Container(
//      child: PdfViewer(
//        filePath: widget.path,
//      ),
//    );
//  }

//  _buildButtonNavigation() {
//    return FutureBuilder<PDFViewController>(
//      future: _controller.future,
//      builder: (context, AsyncSnapshot<PDFViewController> snapshot) {
//        if (snapshot.hasData) {
//          return FloatingActionButton.extended(
//            label: Text("Go to ${pages ~/ 2}"),
//            onPressed: () async {
//              await snapshot.data.setPage(pages ~/ 2);
//            },
//            backgroundColor: TratoColors.orange,
//          );
//        }
//
//        return Container();
//      },
//    );
//  }
}
