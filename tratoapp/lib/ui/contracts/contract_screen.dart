import 'dart:async';
import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:nice_button/nice_button.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/manager/show_contract_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/pdf_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/services/pdf_service_provider.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class ContractScreen extends StatefulWidget {
  final bool isSignPreview;
  final ContractModel contractModel;

  ContractScreen({@required this.isSignPreview, @required this.contractModel});

  @override
  _ContractState createState() => _ContractState();
}

class _ContractState extends State<ContractScreen>
    with AfterLayoutMixin<ContractScreen> {
  String assetPDFPath = "";
  String urlPDFPath;
  bool _loading = false;
  String localPath;

  final BehaviorSubject<ApiResponse> _subjectContractScreen =
      BehaviorSubject<ApiResponse>();

  @override
  void afterFirstLayout(BuildContext context) {
    ShowContractManager()
        .show(_subjectContractScreen, widget.contractModel.sId, {});
    _subjectContractScreen.listen((apiResponse) {
      if (apiResponse.model != null) {
        PdfModel model = apiResponse.model;
        urlPDFPath = model.url;
        _initPdf();
      }
    });
  }

  Future<void> _initPdf() async {
    ApiServiceProvider.loadPDF(urlPDFPath, false).then((value) {
      setState(() {
        localPath = value;
        this._loading = false;
      });
    });

    this.setState(() {
      this._loading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          ExtendedNavigator.rootNavigator.pushNamedAndRemoveUntil(
              Routes.contractListScreen, (route) => false,
              arguments: ContractListScreenArguments(isBackFromContract: true));
        },
        child: Scaffold(
            appBar: AppBar(
              elevation: 10,
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                    child: Image.asset(
                      ImageConstants.mainLogo,
                      height: 30.0,
                      width: 120.0,
                    ))
              ],
              leading: IconButton(
                iconSize: 40,
                icon: Icon(CupertinoIcons.back),
                color: Colors.black54,
                onPressed: () => ExtendedNavigator.rootNavigator
                    .pushNamedAndRemoveUntil(
                        Routes.contractListScreen, (route) => false,
                        arguments: ContractListScreenArguments(
                            isBackFromContract: true)),
              ),
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              )),
            ),
            bottomNavigationBar: SafeArea(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                  Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 30, right: 30, top: 20, bottom: 40),
                      child: NiceButton(
                        elevation: 8,
                        radius: 8,
                        background: TratoColors.orange,
                        text: !widget.isSignPreview
                            ? StringConstants.next
                            : StringConstants.procccedToSign,
                        fontSize: 18,
                        onPressed: () {
                          _navigateNextScreen();
                        },
                      ),
                    ),
                  )
                ])),
            body: _loading
                ? Center(
                    child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(TratoColors.orange),
                  ))
                : Padding(
                    padding: EdgeInsets.all(10),
                    child: Card(
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        )),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: CustomText(
                                      customText: widget.contractModel.name,
                                      align: TextAlign.start,
                                      textColor: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                              Container(
                                  height: 300,
                                  child: PDFView(
                                    filePath: localPath,
                                  ))
                            ])))));
  }

  void _navigateNextScreen() {
    if (widget.contractModel.statuses.participant.shield4DataRequested) {
      ExtendedNavigator.rootNavigator.pushNamed(Routes.fingerprintScreen,
          arguments:
              FingerprintScreenArguments(contractModel: widget.contractModel));
    } else {
      if (widget.contractModel.statuses.participant.requestAgreement) {
        if (widget.isSignPreview) {
          ExtendedNavigator.rootNavigator.pushNamed(Routes.signatureScreen,
              arguments: SignatureScreenArguments(
                  contractModel: widget.contractModel));
        } else {
          ExtendedNavigator.rootNavigator.pushNamed(Routes.videoCaptureScreen,
              arguments: VideoCaptureScreenArguments(
                  contractModel: widget.contractModel));
        }
      } else {
        if (widget.contractModel.status == StringConstants.status021 ||
            widget.contractModel.status == StringConstants.status040 ||
            /**Blindaje 1**/
            widget.contractModel.status == StringConstants.status038 ||
            widget.contractModel.status == StringConstants.status027) {
          widget.contractModel.shield3ValidationRequired = false;
          ExtendedNavigator.rootNavigator.pushNamed(
              Routes.successValidationScreen,
              arguments: SuccessValidationScreenArguments(
                  contractModel: widget.contractModel));
          // } else if (contractModel.status == StringConstants.status013 ||
          //     contractModel.status == StringConstants.status028 ||
          //     contractModel.status == StringConstants.status015) {
          //   this.showAlertNoAvailableContract();
          //   GetContractManager().removeContract(contractLink);
        } else {
          ExtendedNavigator.rootNavigator.pushNamed(Routes.signatureScreen,
              arguments: SignatureScreenArguments(
                  contractModel: widget.contractModel));
        }
      }
    }
  }
}
