import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/strings.dart';

class AboutUsScreen extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(StringConstants.aboutUs),
          backgroundColor: TratoColors.orange,
        ),
        body: Container());
  }
}
