import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/manager/get_contract_manager.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/session_jwt.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';
import 'package:tratoapp/utils/utilities.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

enum UniLinksType { string, uri }

class _SplashState extends State<SplashScreen> with AfterLayoutMixin {
  String _latestLink = 'Unknown';
  Uri _latestUri;
  UniLinksType _type = UniLinksType.string;
  StreamSubscription _sub;
  bool _isLoading = false;
  final BehaviorSubject<ApiResponse> _subjectSessionJwt =
      BehaviorSubject<ApiResponse>();
  final BehaviorSubject<ApiResponse> _subjectContract =
      BehaviorSubject<ApiResponse>();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _subjectContract.close();
    _subjectSessionJwt.close();
  }

  @override
  void initState() {
    super.initState();
//    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildLogo(),
          _buildLoadingView(),
        ],
      ),
    ));
  }

  _buildLogo() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10.0),
      child: Image.asset(ImageConstants.mainLogo, height: 60.0),
    );
  }

  _buildLoadingView() {
    return _isLoading
        ? CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(TratoColors.orange),
          )
        : Empty();
  }

  // URI
//  initPlatformStateForUri() async {
//    Uri initialUri;
//    String initialLink;
//
//    _sub = getUriLinksStream().listen((Uri uri) {
//      if (!mounted) return;
//      setState(() {
//        _latestUri = uri;
//        _latestLink = uri?.toString() ?? 'Unknown';
//      });
//    }, onError: (err) {
//      if (!mounted) return;
//      setState(() {
//        _latestUri = null;
//        _latestLink = 'Failed to get latest link: $err.';
//      });
//    });
//
//    // Attach a second listener to the stream
//    getUriLinksStream().listen((Uri uri) {
//      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
//    }, onError: (err) {
//      print('got err: $err');
//    });
//
//    // PlatformException.
//    try {
//      initialUri = await getInitialUri();
//      print('initial uri: ${initialUri?.path}'
//          ' ${initialUri?.queryParametersAll}');
//      initialLink = initialUri?.toString();
//    } on PlatformException {
//      initialUri = null;
//      initialLink = 'Failed to get initial uri.';
//    } on FormatException {
//      initialUri = null;
//      initialLink = 'Bad parse the initial link as Uri.';
//    }
//
//    //set final state
//    if (!mounted) return;
//
//    setState(() {
//      _latestUri = initialUri;
//      _latestLink = initialLink;
//    });
//  }

  void launchExternalLink(String link) {
    setState(() {
      this._isLoading = true;
    });

    SessionManager().getSessionByLink(_subjectSessionJwt, link);
    String contractId = Utilities().getContractId(link);

    this._subjectSessionJwt.stream.listen((apiResponse) {
      if (apiResponse.model != null) {
        SessionJwt session = apiResponse.model;
        if (session.success) {
          SessionManager().setSessionToken(session.jwt);
          GetContractManager().show(_subjectContract, contractId, {});
        } else {}
      }
    });

    this._subjectContract.stream.listen((apiResponse) {
      this.setState(() {
        this._isLoading = false;
      });
      if (apiResponse.model != null) {
        ContractModel model = apiResponse.model;
        if (model.statuses.participant.requestAgreement) {
          GetContractManager().setCurrentContract(model);
          ExtendedNavigator.rootNavigator.pushReplacementNamed(
              Routes.contractScreen,
              arguments: ContractScreenArguments(
                  isSignPreview: false, contractModel: model));
        } else {
          Navigation.showToast(StringConstants.armour3_no_available, context);
        }
      } else {
        Navigation.showToast(StringConstants.contract_no_available, context);
      }
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      if (!_isLoading) {
        ExtendedNavigator.rootNavigator
            .pushReplacementNamed(Routes.contractListScreen);
      }
    });
  }
}
