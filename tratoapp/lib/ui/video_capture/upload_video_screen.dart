import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//import 'package:flutter_video_compress/flutter_video_compress.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/manager/relate_video_manager.dart';
import 'package:tratoapp/manager/upload_video_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/relate_video_model.dart';
import 'package:tratoapp/model/upload_video_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/navigation_utils.dart';
import 'package:tratoapp/utils/strings.dart';

// ignore: must_be_immutable
class UploadVideoScreen extends StatefulWidget {
  final String videoPath;
  final ContractModel contractModel;

  UploadVideoScreen({@required this.videoPath, @required this.contractModel});

  @override
  _UploadVideoState createState() => _UploadVideoState();
}

class _UploadVideoState extends State<UploadVideoScreen>
    with AfterLayoutMixin<UploadVideoScreen> {
  bool isUploadingVideo = false;

//  Subscription _subscription;
//  final _flutterVideoCompress = FlutterVideoCompress();

//  MediaInfo _originalVideoInfo = MediaInfo(path: '');
//  MediaInfo _compressedVideoInfo = MediaInfo(path: '');
  String _taskName;
  double _progressState = 0;

//  final _loadingStreamCtrl = StreamController<bool>.broadcast();

  final BehaviorSubject<ApiResponse> _subjectUploadVideo =
      BehaviorSubject<ApiResponse>();

  final BehaviorSubject<ApiResponse> _subjectRelateVideo =
      BehaviorSubject<ApiResponse>();

  @override
  void initState() {
    super.initState();
    this.setState(() {
      this.isUploadingVideo = true;
    });
  }

  @override
  void dispose() {
    super.dispose();
//    _subscription.unsubscribe();
//    _loadingStreamCtrl.close();
    _subjectRelateVideo.close();
    _subjectUploadVideo.close();
  }

  @override
  void afterFirstLayout(BuildContext context) {
//    Timer(Duration(seconds: 5), () {
//      this.setState(() {
//        this.isUploadingVideo = false;
//      });
//    });
    this.compressVideo(widget.videoPath);

    _subjectUploadVideo.stream.listen((apiResponse) {
      if (apiResponse.model != null) {
        UploadVideoModel model = apiResponse.model;
        if (model.success) {
          RelateVideoManager().relateVideo(
              _subjectRelateVideo, model.url, widget.contractModel.sId);
        } else {
          ExtendedNavigator.rootNavigator.pop();
          Navigation.showToast(StringConstants.error_upload_video, context);
        }
      }
    });

    _subjectRelateVideo.stream.listen((apiResponse) {
      if (apiResponse.model != null) {
        RelateVideoModel model = apiResponse.model;
        if (model.success) {
          setState(() {
            isUploadingVideo = false;
            if (!widget.contractModel.shield3ValidationRequired) {
              ExtendedNavigator.rootNavigator.pushNamed(Routes.signatureScreen,
                  arguments: SignatureScreenArguments(
                      contractModel: widget.contractModel));
            }
          });
        } else {
          Navigation.showToast(StringConstants.error_relate_video, context);
          ExtendedNavigator.rootNavigator.pop();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            backgroundColor: Colors.white,
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          body: SingleChildScrollView(
              child: isUploadingVideo
                  ? MainCustomCard(
                      icon: ImageConstants.clockIcon,
                      title: StringConstants.title_uploading_video,
                      content: StringConstants.content_uploading_video,
                      isLoading: true,
                    )
                  : MainCustomCard(
                      icon: ImageConstants.mailIcon,
                      title: StringConstants.title_success_uploading_video,
                      content: StringConstants.content_success_uploading_video,
                      isLoading: false,
                    )),
          bottomNavigationBar: !isUploadingVideo
              ? SafeArea(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                      Card(
                        elevation: 20.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0),
                        )),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 30, right: 30, top: 20, bottom: 40),
                          child: NiceButton(
                            elevation: 8,
                            radius: 8,
                            background: TratoColors.orange,
                            text: "Continuar",
                            fontSize: 18,
                            onPressed: () {
                              if (widget
                                  .contractModel.shield3ValidationRequired) {
                                ExtendedNavigator.rootNavigator.pushNamed(
                                    Routes.waitingValidationScreen,
                                    arguments: WaitingValidationScreenArguments(
                                        contractModel: widget.contractModel));
                              } else {
                                ExtendedNavigator.rootNavigator.pushNamed(
                                    Routes.signatureScreen,
                                    arguments: SignatureScreenArguments(
                                        contractModel: widget.contractModel));
                              }
                            },
                          ),
                        ),
                      )
                    ]))
              : Empty(),
        ));
  }

  void uploadVideo(String video64) {
    UploadVideoManager().uploadVideo(_subjectUploadVideo, video64);
  }

  compressVideo(String videoPath) async {
//    _loadingStreamCtrl.sink.add(true);
    String path = videoPath.substring(7);
//    final compressedVideoInfo = await _flutterVideoCompress.compressVideo(
//      path,
//      quality: VideoQuality.DefaultQuality,
//      deleteOrigin: false,
//    );
//    _taskName = null;
//    print(compressedVideoInfo.path);
//    _subscription =
//        _flutterVideoCompress.compressProgress$.subscribe((progress) {
//      setState(() {
//        _progressState = progress;
//      });
//    });

    this.decodeVideo(path);
  }

  decodeVideo(newVideoPath) {
    final bytes = File(newVideoPath).readAsBytesSync();
    String video64 = base64Encode(bytes);
    uploadVideo(video64);
  }
}
