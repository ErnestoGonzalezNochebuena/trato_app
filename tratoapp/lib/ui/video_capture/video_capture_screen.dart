import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/NiceButton.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:tratoapp/components/main_custom_card.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';

class VideoCaptureScreen extends StatefulWidget {
  const VideoCaptureScreen({@required this.contractModel});

  @override
  _VideoCaptureScreen createState() => _VideoCaptureScreen();

  final ContractModel contractModel;
}

class _VideoCaptureScreen extends State<VideoCaptureScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          ExtendedNavigator.rootNavigator.pop();
        },
        child: Scaffold(
            appBar: AppBar(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              )),
              leading: IconButton(
                iconSize: 40,
                icon: Icon(CupertinoIcons.back),
                color: Colors.black54,
                onPressed: () {
                  ExtendedNavigator.rootNavigator.pop();
                },
              ),
              backgroundColor: Colors.white,
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                    child: Image.asset(
                      ImageConstants.mainLogo,
                      height: 30.0,
                      width: 120.0,
                    ))
              ],
            ),
            bottomNavigationBar: SafeArea(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                  Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 30, right: 30, top: 20, bottom: 40),
                      child: NiceButton(
                        elevation: 8,
                        radius: 8,
                        background: TratoColors.orange,
                        text: "Iniciar grabación",
                        fontSize: 18,
                        onPressed: () {
                          this.checkPermissions();
                        },
                      ),
                    ),
                  )
                ])),
            body: SingleChildScrollView(
                child: MainCustomCard(
                    icon: ImageConstants.videoIcon,
                    title: StringConstants.armour3_title1,
                    content: StringConstants.armour3_content1))));
  }

  Future<void> checkPermissions() async {
    if (Platform.isAndroid) {
      PermissionStatus permissionExternalStorageResult =
          await SimplePermissions.requestPermission(
              Permission.WriteExternalStorage);
      PermissionStatus permissionRecordResult =
          await SimplePermissions.requestPermission(Permission.RecordAudio);
      PermissionStatus permissionCameraResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionExternalStorageResult == PermissionStatus.authorized &&
          permissionRecordResult == PermissionStatus.authorized &&
          permissionCameraResult == PermissionStatus.authorized) {
        ExtendedNavigator.rootNavigator.pushNamed(Routes.cameraOverlayScreen,
            arguments: CameraOverlayScreenArguments(
              contractModel: widget.contractModel,
            ));
      } else {
        this.showAlertPermissions();
      }
    } else if (Platform.isIOS) {
      PermissionStatus permissionRecordResult =
          await SimplePermissions.requestPermission(Permission.RecordAudio);
      PermissionStatus permissionCameraResult =
          await SimplePermissions.requestPermission(Permission.Camera);
      if (permissionRecordResult == PermissionStatus.authorized &&
          permissionCameraResult == PermissionStatus.authorized) {
        ExtendedNavigator.rootNavigator.pushNamed(Routes.cameraOverlayScreen,
            arguments: CameraOverlayScreenArguments(
              contractModel: widget.contractModel,
            ));
      } else {
        this.showAlertPermissions();
      }
    }
  }

  void showAlertPermissions() {
    showDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
              title: Text(StringConstants.alert),
              content: Text(StringConstants.alertPermissions),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("Dar permisos"),
                  isDefaultAction: true,
                  onPressed: () async {
                    ExtendedNavigator.rootNavigator.pop();
                    if (Platform.isIOS) {
                      PermissionStatus permissionRecordResult =
                          await SimplePermissions.requestPermission(
                              Permission.RecordAudio);
                      PermissionStatus permissionCameraResult =
                          await SimplePermissions.requestPermission(
                              Permission.Camera);
                      if (permissionRecordResult !=
                          PermissionStatus.authorized) {
                        AppSettings.openAppSettings();
                        return;
                      }
                      if (permissionCameraResult !=
                          PermissionStatus.authorized) {
                        AppSettings.openSoundSettings();
                        return;
                      }
                    } else {
                      this.checkPermissions();
                    }
                  },
                ),
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text("Cancelar"),
                  onPressed: () {
                    ExtendedNavigator.rootNavigator.pop();
                  },
                )
              ],
            ),
        barrierDismissible: true);
  }
}
