import 'dart:io';
import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_button/nice_button.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tratoapp/components/circle_button.dart';
import 'package:tratoapp/components/custom_text.dart';
import 'package:tratoapp/components/empty_view.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/router/router.gr.dart';
import 'package:tratoapp/utils/colors.dart';
import 'package:tratoapp/utils/images.dart';
import 'package:tratoapp/utils/strings.dart';
import 'package:video_player/video_player.dart';

class CameraOverlayScreen extends StatefulWidget {
  final ContractModel contractModel;

  CameraOverlayScreen({@required this.contractModel});

  @override
  _CameraOverlayState createState() => _CameraOverlayState();
}

class _CameraOverlayState extends State<CameraOverlayScreen>
    with WidgetsBindingObserver {
  String videoPath;
  VideoPlayerController videoController;
  VideoPlayerController previewController;
  CameraController controller;
  VoidCallback videoPlayerListener;
  bool enableAudio = true;
  List<CameraDescription> cameras = [];
  String filePath;
  bool havePreviewRecord = false;

  @override
  void initState() {
    super.initState();
    availableCameras().then((value) {
      setState(() {
        cameras.addAll(value);
        onNewCameraSelected(cameras[1]);
      });
    });
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    videoController?.dispose();
    previewController?.dispose();
    controller?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        // ignore: missing_return
        onWillPop: () {
          showDialog(
              context: context,
              builder: (_) => CupertinoAlertDialog(
                    title: Text(StringConstants.alert),
                    content: Text(StringConstants.cancelVideoRecord),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child: Text("Si"),
                        isDefaultAction: true,
                        onPressed: () {
                          ExtendedNavigator.rootNavigator.pop();
                          ExtendedNavigator.rootNavigator.pop();
                        },
                      ),
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        child: Text("No"),
                        onPressed: () {
                          ExtendedNavigator.rootNavigator.pop();
                        },
                      )
                    ],
                  ),
              barrierDismissible: true);
        },
        child: Scaffold(
          appBar: AppBar(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            )),
            leading: IconButton(
              iconSize: 40,
              icon: Icon(CupertinoIcons.back),
              color: Colors.black54,
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) => CupertinoAlertDialog(
                          title: Text(StringConstants.alert),
                          content: Text(StringConstants.cancelVideoRecord),
                          actions: <Widget>[
                            CupertinoDialogAction(
                              child: Text("Si"),
                              isDefaultAction: true,
                              onPressed: () {
                                ExtendedNavigator.rootNavigator.pop();
                                ExtendedNavigator.rootNavigator.pop();
                              },
                            ),
                            CupertinoDialogAction(
                              isDefaultAction: true,
                              child: Text("No"),
                              onPressed: () {
                                ExtendedNavigator.rootNavigator.pop();
                              },
                            )
                          ],
                        ),
                    barrierDismissible: true);
              },
            ),
            backgroundColor: Colors.white,
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 10.0, bottom: 10.0),
                  child: Image.asset(
                    ImageConstants.mainLogo,
                    height: 30.0,
                    width: 120.0,
                  ))
            ],
          ),
          bottomNavigationBar: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                Card(
                    elevation: 20.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    )),
                    child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CustomText(
                                customText: StringConstants.readNextLines,
                                textColor: Colors.black54,
                                fontSize: 18,
                                align: TextAlign.start,
                                fontWeight: FontWeight.w500),
                            SizedBox(
                              height: 10,
                            ),
                            CustomText(
                              fontSize: 15,
                              customText: widget.contractModel.statuses
                                      .agreementMessage ??
                                  '',
                              textColor: Colors.black54,
                              align: TextAlign.start,
                              fontWeight: FontWeight.w100,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            !havePreviewRecord
                                ? _captureControlRowWidget()
                                : Empty()
                          ],
                        )))
              ])),
          key: _scaffoldKey,
          body: videoController != null && videoController.value.size != null
              ? buildVideoPreview()
              : Stack(
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Center(
                          child: _cameraPreviewWidget(),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        border: Border.all(
                          color: controller != null &&
                                  controller.value.isRecordingVideo
                              ? Colors.redAccent
                              : Colors.grey,
                          width: 5.0,
                        ),
                      ),
                    ),
                  ],
                ),
        ));
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Para realizar la grabación es necesario otorgue los '
        '\npermisos solicitados por la Aplicación de Trato.',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  /// Display the control bar with buttons to take pictures and record videos.
  Widget _captureControlRowWidget() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Visibility(
                  visible: controller != null &&
                      controller.value.isInitialized &&
                      !controller.value.isRecordingVideo,
                  child: CircleButton(
                    iconData: Icons.videocam,
//                color: TratoColors.orange,
                    onTap: controller != null &&
                            controller.value.isInitialized &&
                            !controller.value.isRecordingVideo
                        ? onVideoRecordButtonPressed
                        : null,
                  )),
              Visibility(
                  visible: controller != null &&
                      controller.value.isInitialized &&
                      controller.value.isRecordingVideo,
                  child: CircleButton(
                    iconData:
                        controller != null && controller.value.isRecordingPaused
                            ? Icons.play_arrow
                            : Icons.pause,
//                color: TratoColors.orange,
                    onTap: controller != null &&
                            controller.value.isInitialized &&
                            controller.value.isRecordingVideo
                        ? (controller != null &&
                                controller.value.isRecordingPaused
                            ? onResumeButtonPressed
                            : onPauseButtonPressed)
                        : null,
                  )),
              Visibility(
                  visible: controller != null &&
                      controller.value.isInitialized &&
                      controller.value.isRecordingVideo,
                  child: SizedBox(width: 30.0)),
              Visibility(
                  visible: controller != null &&
                      controller.value.isInitialized &&
                      controller.value.isRecordingVideo,
                  child: CircleButton(
                    iconData: Icons.stop,
//                color: Colors.red,
                    onTap: controller != null &&
                            controller.value.isInitialized &&
                            controller.value.isRecordingVideo
                        ? onStopButtonPressed
                        : null,
                  ))
            ],
          ),
          SizedBox(height: 10)
        ]));
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: true,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
//        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((String path) {
      this.filePath = path;
      if (mounted) setState(() {});
//      if (filePath != null) showInSnackBar('Saving video to $filePath');
    });
  }

  void onStopButtonPressed() {
    stopVideoRecording().then((_) {
      if (mounted)
        setState(() {
          this.havePreviewRecord = true;
        });
//      showInSnackBar('Video recorded to: $videoPath');
    });
  }

  void onPauseButtonPressed() {
    pauseVideoRecording().then((_) {
      if (mounted) setState(() {});
//      showInSnackBar('Video recording paused');
    });
  }

  void onResumeButtonPressed() {
    resumeVideoRecording().then((_) {
      if (mounted) setState(() {});
//      showInSnackBar('Video recording resumed');
    });
  }

  Future<String> startVideoRecording() async {
    if (!controller.value.isInitialized) {
//      showInSnackBar('Error: select a camera first.');
      return null;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Movies/flutter_armour3';
    await Directory(dirPath).create(recursive: true);
    this.filePath = '$dirPath/${timestamp()}.mp4';
    prefs.setString('filePathTemp', filePath);

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      await controller.startVideoRecording(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  Future<void> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    await _startVideoPlayer();
  }

  Future<void> pauseVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.pauseVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> resumeVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.resumeVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> _startVideoPlayer() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String path = prefs.getString('filePathTemp');
    this.previewController = VideoPlayerController.file(File(path));
    print(path);
    videoPlayerListener = () {
      if (videoController != null && videoController.value.size != null) {
        // Refreshing the state to update video player with the correct ratio.
        if (mounted) setState(() {});
        videoController.removeListener(videoPlayerListener);
      }
    };
    previewController.addListener(videoPlayerListener);
    await previewController.setLooping(true);
    await previewController.initialize();
    await videoController?.dispose();
    if (mounted) {
      setState(() {
        videoController = previewController;
      });
    }
    await previewController.play();
  }

  Future<void> _stopVideoPlayer(bool isContinue) async {
    await previewController?.pause();
    if (isContinue) {
      ExtendedNavigator.rootNavigator.pushNamed(Routes.uploadVideoScreen,
          arguments: UploadVideoScreenArguments(
              videoPath: videoController.dataSource,
              contractModel: widget.contractModel));
    } else {
      this.setState(() {
        this.havePreviewRecord = false;
      });
    }
//    await previewController?.dispose();
  }

  void _showCameraException(CameraException e) {
//    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  buildVideoPreview() {
    return Stack(children: [
      Container(
        child: Center(
          child: AspectRatio(
              aspectRatio: previewController.value.aspectRatio,
              child: VideoPlayer(videoController)),
        ),
      ),
      Align(
          alignment: Alignment.bottomCenter,
          child: Wrap(spacing: 5.0, runSpacing: 5.0, children: [
            NiceButton(
              elevation: 8,
              radius: 8,
              fontSize: 14,
              width: 140,
              text: StringConstants.done,
              background: TratoColors.orange,
              textColor: Colors.white,
              onPressed: () {
                _stopVideoPlayer(true);
              },
            ),
            SizedBox(width: 10),
            NiceButton(
              fontSize: 14,
              elevation: 8,
              radius: 8,
              width: 140,
              text: StringConstants.startAgain,
              background: Colors.white,
              textColor: TratoColors.orange,
              onPressed: () {
                this.setState(() {
                  _stopVideoPlayer(false);
                  this.videoController = null;
                });
              },
            ),
            SizedBox(height: 10),
          ])),
    ]);
  }
}
