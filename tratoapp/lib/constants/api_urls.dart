class Urls {
  static String endpoint() {
    if (devEnv())
      return "enterprise-test.api.trato.io";
    else
      return "enterprise.api.trato.io";
  }

  static bool devEnv() {
    return false;
  }

  static const String getSessionJwt = "/contracts/session/get";
  static const String getContract = "/contracts/client";
  static const String convertContractToPdf = "/contracts/external/pdf";
  static const String uploadVideo = "/video/acceptance";
  static const String relateVideo = "/client/contracts/";
  static const String uploadSignature = "/client/contracts/";
  static const String uploadFingerprints = "/client/contracts/";
  static const String acceptAgreement = "/client/contracts/";
}

enum HttpMethods { GET, POST, PUT, DELETE, PATCH }
