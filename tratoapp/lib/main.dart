import 'package:auto_route/auto_route.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:nice_button/generated/i18n.dart';
import 'package:tratoapp/router/router.gr.dart' as r;
import 'package:tratoapp/utils/colors.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  availableCameras();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _Main createState() => _Main();
}

class _Main extends State<MyApp> with SingleTickerProviderStateMixin {
  List<LocalizationsDelegate> delegates = List<LocalizationsDelegate>();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent, // status bar color
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light));
    delegates = [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      DefaultCupertinoLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      FallbackCupertinoLocalisationsDelegate(),
      S.delegate
//      _.S.delegate
    ];
  }

  final generalTheme = ThemeData(
      fontFamily: 'Futura',
      textTheme: TextTheme(
          headline5: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w300, // Futura-Book
              color: TratoColors.greyishBrown),
          bodyText2: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w200, // Futura-Light
              color: TratoColors.greyishBrown),
          bodyText1: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w400, // Futura-Book
              color: TratoColors.gray)));

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Trato io',
        theme: generalTheme,
        localizationsDelegates: delegates,
        supportedLocales: [
          const Locale('en', ''),
          const Locale('es', ''),
        ],
        debugShowCheckedModeBanner: false,
        builder: ExtendedNavigator<r.Router>(
          router: r.Router(),
        ));
  }
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}
