import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/upload_video_model.dart';

class UploadVideoManager extends Manager {
  static final UploadVideoManager _singleton = UploadVideoManager._internal();
  Model Function(dynamic) constructor =
      (json) => UploadVideoModel.fromJson(json);
  String resourcePath = Urls.uploadVideo;

  factory UploadVideoManager() {
    return _singleton;
  }

  Future<ApiResponse> uploadVideo(
      BehaviorSubject<ApiResponse> _subject, String video64) {
    return super.apiRequest(
        _subject,
        HttpMethods.POST,
        UploadVideoModel.toJson(video64),
        null,
        Urls.uploadVideo,
        (json) => UploadVideoModel.fromJson(json));
  }

  UploadVideoManager._internal();
}
