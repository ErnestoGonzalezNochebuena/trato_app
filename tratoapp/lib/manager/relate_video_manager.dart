import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/relate_video_model.dart';
import 'package:tratoapp/model/upload_video_model.dart';

class RelateVideoManager extends Manager {
  static final RelateVideoManager _singleton = RelateVideoManager._internal();
  Model Function(dynamic) constructor =
      (json) => RelateVideoModel.fromJson(json);
  String resourcePath = Urls.relateVideo;

  factory RelateVideoManager() {
    return _singleton;
  }

  Future<ApiResponse> relateVideo(BehaviorSubject<ApiResponse> _subject,
      String url, String contractId) async {
    return super.apiRequest(
        _subject,
        HttpMethods.POST,
        RelateVideoModel().toJson(url),
        null,
        "${Urls.relateVideo}$contractId/consent",
        (json) => RelateVideoModel.fromJson(json));
  }

  RelateVideoManager._internal();
}
