import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/base_model.dart';

class AcceptAgreementManager extends Manager {
  static final AcceptAgreementManager _singleton =
      AcceptAgreementManager._internal();
  Model Function(dynamic) constructor = (json) => Model.fromJson(json);
  String resourcePath = Urls.uploadVideo;

  factory AcceptAgreementManager() {
    return _singleton;
  }

  AcceptAgreementManager._internal();
}
