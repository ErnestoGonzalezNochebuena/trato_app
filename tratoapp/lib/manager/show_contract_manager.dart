import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/pdf_model.dart';

class ShowContractManager extends Manager {
  static final ShowContractManager _singleton = ShowContractManager._internal();
  Model Function(dynamic) constructor = (json) => PdfModel.fromJson(json);
  String resourcePath = Urls.convertContractToPdf;

  factory ShowContractManager() {
    return _singleton;
  }

  ShowContractManager._internal();
}
