import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/model/signature_model/signature_model.dart';
import 'package:tratoapp/model/signature_request/signature_request.dart';

class SignatureRepository extends Manager {
  static final SignatureRepository _singleton = SignatureRepository._internal();
  String resourcePath = Urls.uploadVideo;

  factory SignatureRepository() {
    return _singleton;
  }

  void uploadSignature(BehaviorSubject<SignatureModel> _subject,
      SignatureRequest request, String contractId, String participantId) async {
    var dio = Dio(BaseOptions(baseUrl: 'https://${Urls.endpoint()}'));

    var sessionToken = await SessionManager().getSessionToken();
    var jsonRequest = jsonEncode(request.toJson());
    var result = SignatureModel();
    try {
      var response = await dio.post(
          '${Urls.uploadSignature}$contractId/sign/participant/$participantId',
          data: jsonRequest,
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            'jwt': sessionToken,
          }));
      if (response.statusCode == 200) {
        result = SignatureModel.fromJson(response.data);
        print(response.data.toString());
      } else {
        result = SignatureModel(success: false, error: "Error en la respuesta");
        print(jsonRequest);
      }
    } on DioError catch (e) {
      print(e.toString());
      result = SignatureModel(success: false, error: "Error en la respuesta");
    }

    _subject.sink.add(result);
  }

  SignatureRepository._internal();
}
