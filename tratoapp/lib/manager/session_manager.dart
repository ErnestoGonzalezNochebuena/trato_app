import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/session_jwt.dart';

class SessionManager extends Manager {
  static final SessionManager _singleton = SessionManager._internal();
  Model Function(dynamic) constructor = (json) => SessionJwt.fromJson(json);
  String resourcePath = Urls.getSessionJwt;
  String sessionToken; //Caché para el sesion token

  factory SessionManager() {
    return _singleton;
  }

  setSessionToken(String token) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    await _preference.setString("session_token", token);
    sessionToken = token;
  }

  Future<String> getSessionToken() async {
    if (sessionToken == null) {
      SharedPreferences pref = await SharedPreferences.getInstance();
      sessionToken = await pref.get("session_token");
    }
    return sessionToken;
  }

  Future<bool> hasSessionToken() async {
    String token = await getSessionToken();
    return token != null;
  }

  Future<ApiResponse> getSessionByLink(
      BehaviorSubject<ApiResponse> _subject, String path) async {
    return super.apiRequest(
        _subject,
        HttpMethods.POST,
        SessionJwt().toJson(path),
        null,
        Urls.getSessionJwt,
        (json) => SessionJwt.fromJson(json));
  }

  SessionManager._internal();
}
