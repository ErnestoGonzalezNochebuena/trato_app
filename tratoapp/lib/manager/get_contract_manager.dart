import 'dart:convert';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/manager/show_contract_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/model/contracts_indicator_model.dart';
import 'package:tratoapp/model/session_jwt.dart';
import 'package:tratoapp/utils/utilities.dart';

class GetContractManager extends Manager {
  static final GetContractManager _singleton = GetContractManager._internal();
  Model Function(dynamic) constructor = (json) => ContractModel.fromJson(json);
  String resourcePath = Urls.getContract;
  String sessionToken; //Caché para el sesion token

  factory GetContractManager() {
    return _singleton;
  }

  setCurrentContract(ContractModel model) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    String result = jsonEncode(model);
    await _preference.setString("current_contract", result);
  }

  Future<ContractModel> getCurrentContract() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String json = await pref.get("current_contract");
    ContractModel model = ContractModel.fromJson(jsonDecode(json));
    return model;
  }

  addContract(String contractLink) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    getContracts().then((list) async {
      if (list.isNotEmpty) {
        list.removeWhere((element) => element == contractLink);
      }
      list.add(contractLink);
      String contractList = jsonEncode(list);
      await _preference.setString("contract_list", contractList);
    });
  }

  removeContract(String contractLink) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    getContracts().then((list) async {
      list.removeWhere((element) => element == contractLink);
      String contractList = jsonEncode(list);
      await _preference.setString("contract_list", contractList);
    });
  }

  Future<List<String>> getContracts() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    String list = await _preference.get("contract_list");
    List<String> result = List();
    if (list == null) {
      return result;
    } else {
      List<dynamic> jsonResponse = json.decode(list);
      jsonResponse.forEach((element) {
        result.add(element);
      });
      return result;
    }
//      List<String> contracts =
//          responseJsonList.map((json) => ContractModel.fromJson(json)).toList()
  }

  void loadContracts(BehaviorSubject<ApiResponse> subjectLoadContracts) {
    List<ContractModel> result = List();
    ApiResponse finalResult = new ApiResponse();
    IndicatorsModel indicatorModel = IndicatorsModel();
    this.getContracts().then((links) async {
      if (links.isNotEmpty) {
        indicatorModel.lenght = links.length + 1;
        indicatorModel.percentageSection = 100 ~/ (links.length + 1);
        indicatorModel.currentStep = 20;
        finalResult.model = indicatorModel;
        subjectLoadContracts.sink.add(finalResult);
      }
      for (String contractLink in links) {
        await super
            .apiRequest(
                BehaviorSubject<ApiResponse>(),
                HttpMethods.POST,
                SessionJwt().toJson(contractLink),
                null,
                Urls.getSessionJwt,
                (json) => SessionJwt.fromJson(json))
            .then((value) async {
          await SessionManager()
              .setSessionToken((value.model as SessionJwt).jwt);
          await super.show(
              BehaviorSubject<ApiResponse>(),
              Utilities().getContractId(contractLink),
              {}).then((contractModel) {
            ContractModel model = contractModel.model as ContractModel;
            model.jwt = (value.model as SessionJwt).jwt;
            indicatorModel.currentStep = (links.indexOf(contractLink) + 2) *
                indicatorModel.percentageSection;
            finalResult.model = indicatorModel;
            subjectLoadContracts.sink.add(finalResult);
            result.add(contractModel.model as ContractModel);
          });
        });
      }
      if (result.isNotEmpty) {
        Future.delayed(const Duration(milliseconds: 500), () {
          finalResult.models = result;
          subjectLoadContracts.sink.add(finalResult);
        });
      } else {
        finalResult.models = result;
        subjectLoadContracts.sink.add(finalResult);
      }
    });
  }

  GetContractManager._internal();
}
