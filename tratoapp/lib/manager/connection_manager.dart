import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/session_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/multi_part_data.dart';

class Manager {
  String _baseUrl = Urls.endpoint();
  String resourcePath = "";
  Model Function(dynamic) constructor = (json) => Model.fromJson(json);

  Future<ApiResponse> create(
      BehaviorSubject<ApiResponse> subject, Model model, String json) {
    return apiRequest(subject, HttpMethods.POST, model.toApiParams(),
        model.dataUpload(), resourcePath);
  }

  Future<ApiResponse> index(BehaviorSubject<ApiResponse> subject,
      [Map<String, String> params]) {
    return apiRequest(subject, HttpMethods.GET, params, null, resourcePath);
  }

  Future<ApiResponse> show(BehaviorSubject<ApiResponse> subject, String id,
      [Map<String, String> params]) {
    return apiRequest(
        subject, HttpMethods.GET, params, null, resourcePath + "/" + id);
  }

  Future<ApiResponse> destroy(BehaviorSubject<ApiResponse> subject, String id,
      [Map<String, String> params]) {
    return apiRequest(
        subject, HttpMethods.DELETE, params, null, resourcePath + "/" + id);
  }

  Future<ApiResponse> apiRequest(
      BehaviorSubject<ApiResponse> subject,
      HttpMethods method,
      Map<String, dynamic> params,
      Map<String, dynamic> uploadData,
      String resourcePath,
      [Model Function(dynamic) customConstructor]) async {
    if (params == null) params = {};
    final apiResponse =
        await apiRequestDynamic(method, params, uploadData, resourcePath);
    Model Function(dynamic) theModelConstructor =
        customConstructor != null ? customConstructor : constructor;
    if (apiResponse.responseJson != null) {
      apiResponse.model = theModelConstructor(apiResponse.responseJson);
      apiResponse.responseJson = null;
    } else if (apiResponse.responseJsonList != null) {
      apiResponse.models = apiResponse.responseJsonList
          .map((json) => theModelConstructor(json))
          .toList();
      apiResponse.responseJsonList = null;
    }
    try {
      //pueda que el sink ya esté cerrado
      if (subject != null) subject.sink.add(apiResponse);
    } catch (e) {}
    return apiResponse;
  }

  Future<ApiResponse> apiRequestDynamic(
      HttpMethods method,
      Map<String, dynamic> params,
      Map<String, dynamic> uploadData,
      String originalUrl) async {
    ApiResponse apiResponse = ApiResponse();
    try {
      final response =
          await apiRequestRaw(method, params, uploadData, originalUrl);
      var responseBody = "";
      if (response is http.Response)
        responseBody = response.body;
      else if (response is http.StreamedResponse)
        responseBody = await response.stream.bytesToString();
      print("====== " +
          method.toString() +
          " " +
          originalUrl +
          " " +
          response.statusCode.toString() +
          responseBody);

      dynamic jsonResponse = "";
      try {
        jsonResponse = json.decode(responseBody);
      } catch (e) {
        e.toString();
      }

      if (response.statusCode == 200) {
        if (jsonResponse is Map) {
          apiResponse.responseJson = jsonResponse;
        } else if (jsonResponse is List) {
          apiResponse.responseJsonList = jsonResponse;
        }
      } else if (response.statusCode == 422) {
        apiResponse.unprocessableEntityJson = jsonResponse;
        if (jsonResponse["mssg"] != null)
          apiResponse.unprocessableEntityMessage = jsonResponse["mssg"];
      } else {
        apiResponse.error =
            "(" + response.statusCode.toString() + ") " + responseBody;
      }
    } catch (e, s) {
      apiResponse.error = e.toString();
      print(e);
      print(s);
    }
    return apiResponse;
  }

  Future<http.BaseResponse> apiRequestRaw(
      HttpMethods method,
      Map<String, dynamic> params,
      Map<String, MultiPartData> uploadData,
      String originalUrl) async {
    String sessionToken = "";
    Map<String, String> headers = Map();
    if (await SessionManager().hasSessionToken()) {
      sessionToken = await SessionManager().getSessionToken();
      headers = {"jwt": sessionToken};
//      params.addAll({"jwt": sessionToken});
      print("TOKEN   " + sessionToken);
    }

    String fullUrl = method == HttpMethods.GET ||
            method == HttpMethods.DELETE ||
            method == HttpMethods.PUT
        ? _getUrl(params, originalUrl)
        : method == HttpMethods.POST
            ? _getUrl({}, originalUrl)
            : _getUrl({}, originalUrl);

    print("--------" + fullUrl);
    if (uploadData != null && uploadData.isNotEmpty) {
      var request = new http.MultipartRequest(
          method.toString().replaceAll("HttpMethods.", ""), Uri.parse(fullUrl));
      uploadData.forEach((key, multipartData) {
        var multipartFile = new http.MultipartFile(
            key, multipartData.stream, multipartData.length,
            filename: multipartData.name);
        request.files.add(multipartFile);
      });
      request.fields.addAll(params);
      return await request.send();
    }

    print("estas es la url completa " + fullUrl);
    print(params);

    if (method == HttpMethods.GET) {
//      print(fullUrl);
      return await http.get(fullUrl, headers: headers);
    } else if (method == HttpMethods.POST) {
      return await http.post(fullUrl, body: params, headers: headers);
    } else if (method == HttpMethods.PATCH) {
      return await http.patch(fullUrl, body: params, headers: headers);
    } else if (method == HttpMethods.PUT) {
      return await http.put(fullUrl, body: params, headers: headers);
    } else if (method == HttpMethods.DELETE) {
      return await http.delete(fullUrl);
    }
  }

  String _getUrl(Map<String, String> params, String urlService) {
    var uri = new Uri.https(_baseUrl, urlService, params);
    return uri.toString();
  }
}
