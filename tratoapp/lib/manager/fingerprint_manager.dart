import 'dart:convert';

import 'package:rxdart/rxdart.dart';
import 'package:tratoapp/constants/api_urls.dart';
import 'package:tratoapp/manager/connection_manager.dart';
import 'package:tratoapp/model/api_response.dart';
import 'package:tratoapp/model/base_model.dart';
import 'package:tratoapp/model/fingerprint_model.dart';

class FingerprintManager extends Manager {
  static final FingerprintManager _singleton = FingerprintManager._internal();
  Model Function(dynamic) constructor =
      (json) => FingerprintModel.fromJson(json);
  String resourcePath = Urls.uploadVideo;

  factory FingerprintManager() {
    return _singleton;
  }

  Future<ApiResponse> uploadFingerprints(BehaviorSubject<ApiResponse> _subject,
      String fingerprintResponse, String contractId) {
    FingerprintModel request =
        FingerprintModel.fromJson(jsonDecode(fingerprintResponse));
    return super.apiRequest(
        _subject,
        HttpMethods.POST,
        request.toJson(),
        null,
        "${Urls.uploadFingerprints}$contractId/addshield4data",
        (json) => FingerprintModel.fromJson(json));
  }

  FingerprintManager._internal();
}
