class ImageConstants {
  static const String aboutUsInactive = "lib/assets/images/about_us_icon.svg";
  static const String aboutUsActive =
      "lib/assets/images/about_us_icon_active.svg";
  static const String contractActive =
      "lib/assets/images/contract_icon_active.svg";
  static const String contractInactive = "lib/assets/images/contract_icon.svg";
  static const String captureVideoInactive =
      "lib/assets/images/capture_video_icon.svg";
  static const String captureVideoActive =
      "lib/assets/images/capture_video_icon_active.svg";
  static const String fingerprintInactive =
      "lib/assets/images/fingerprint_icon.svg";
  static const String fingerprintActive =
      "lib/assets/images/fingerprint_icon_active.svg";
  static const String videoCaptureAnimation =
      "lib/assets/images/capture_video_animation.json";
  static const String fingerprintAnimation =
      "lib/assets/images/fingerprint_scan.json";
  static const String videoIcon = "lib/assets/images/video.png";
  static const String clockIcon = "lib/assets/images/clock.png";
  static const String mailIcon = "lib/assets/images/mail.png";
  static const String checkIcon = "lib/assets/images/check.png";
  static const String mainLogo = "lib/assets/images/logo_trato.png";
  static const String documentsLogo = "lib/assets/images/documents.png";
  static const String fingerprint = "lib/assets/images/fingerprint.png";
  static const String tratoAnimation =
      "lib/assets/animations/tratoapp_animation.gif";
}
