import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';
import 'package:tratoapp/ui/video_capture/camera_overlay_screen.dart';

class Navigation {
  static void showToast(String msg, BuildContext context) {
    Toast.show(msg, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }
}
