class StringConstants {
  static const String armour3 = "Blindaje 3";
  static const String armour4 = "Blindaje 4";
  static const String aboutUs = "Acerca de";
  static const String documents = "Documentos";

  static const String start = "Iniciar";
  static const String armour3_title1 = "Grabación de video";
  static const String armour3_content1 =
      "El autor de este contrato ha solicitado que para firmar este documento, debes primero grabar una declaración de tu consentimiento. Dale clic al botón continuar para proceder a la grabación.";
  static const String armour3_title2 = "Instrucciones";
  static const String armour3_content2 =
      "Graba el mensaje que se te presentara en la pantalla";

  static const String done = "Continuar";
  static const String startAgain = "Volver a grabar";

  static const String armour4_title1 = "Toma de huellas digitales";
  static const String armour4_content1 =
      "Haremos la toma de huellas digitales para el blindaje 4";
  static const String armour4_title2 = "Instrucciones";
  static const String armour4_content2 =
      "Captura tus huellas digitales a través de la cámara";

  static const String readNextLines = "Lee el siguiente texto:";

  static const String alert = "Alerta";
  static const String cancelVideoRecord =
      "¿Esta seguro de cancelar la grabación del video?";

  static const String title_uploading_video = "Estamos procesando tu video";
  static const String content_uploading_video =
      "En unos instantes podrás continuar";

  static const String title_success_uploading_video =
      "Gracias por enviar tu blindaje";
  static const String content_success_uploading_video =
      "Ahora debes esperar a que el dueño del contrato valide tu video";

  static const String title_waiting_validation = "Están revisando su blindaje";
  static const String content_waiting_validation =
      "Te avisaremos cuando puedas continuar";
  static const String title_success_validation = "Ya validaron tu blindaje";
  static const String content_success_validation =
      "Ahora puedes firmar tu documento";
  static const String content_success_validation2 =
      "Ahora puedes descargar tu contrato";
  static const String title_success_signature = "Listo, has firmado";
  static const String content_success_signature =
      "Gracias por tu firma. Espera a que tu contraparte firme el contrato y podrás descargarlo. Nosotros te avisaremos cuando eso suceda.";
  static const String title_signature = "Traza tu firma.";
  static const String content_signature =
      "Dentro de este recuadro blanco traza tu firma, posteriormente da clic en el botón Firmar.";
  static const String title_no_contracts = "No hay documentos";
  static const String content_no_contracts =
      "En esta bandeja encontrarás los documentos que te hayan enviado a firmar o que hayas escaneado con el lector QR.\n\nSi recibiste un contrato desde la plataforma de TRATO, da clic en tu correo en el botón de \"Ver Contrato\" desde tu teléfono móvil o escanea el código QR de tu contrato.";
  static const String scan_qr = "Escanear código QR";
  static const String armour3_no_available =
      "El blindaje 3 no esta disponible para tu contrato";
  static const String contract_no_available = "Contrato no disponible";

  static var downloadContract = "Descargar contrato";
  static var downloadNom151 = "Descargar NOM-151";
  static var next = "Continuar";

  static var procccedToSign = "Proceder a firma";
  static const String error_upload_video =
      "Ocurrió un error, intente mas tarde.";
  static const String error_relate_video =
      "Ocurrió un error, video no relacionado.";
  static const String content_success_signature2 =
      "Gracias por tu firma. Espera a que tu contraparte firme el contrato y podrás descargarlo. Nosotros te avisaremos cuando eso suceda.";

  static const String message001 = 'Enviado para votación';
  static const String message002 = 'En negociación';
  static const String message003 = 'Es tu turno de firmar';
  static const String message004 = 'Es tu turno de firmar';
  static const String message005 = 'No es tu turno de firmar';
  static const String message006 = 'No es tu turno de firmar';
  static const String message007 = 'Pendiente de Validar';
  static const String message008 = 'Restan';
  static const String message009 = 'día(s)';
  static const String message010 = 'Borrador';
  static const String message011 = 'Aprobado';
  static const String message012 = 'Recibido';
  static const String message013 = 'Pendiente de Completar';
  static const String message014 = 'Enviado con Información';
  static const String message015 = 'Pendiente de Completar';
  static const String message016 = 'Pendiente de Validar';
  static const String message017 = 'Pendiente de Firmar';
  static const String message018 = 'Pendiente de Aceptación';
  static const String message019 = 'Recibido para Revisión';
  static const String message020 = 'Recibido para Aprobación.';
  static const String message021 = 'Firmado';
  static const String message022 = 'Enviado para Aceptación';
  static const String message023 = 'Rechazado';
  static const String message024 = 'Aceptado';
  static const String message025 = 'Pendiente de Completar';
  static const String message026 = 'Pendiente de Validar';
  static const String message027 = 'Enviado a Participantes';
  static const String message028 = 'En Negociación';
  static const String message029 = 'Recibido para Negociar';
  static const String message030 = 'Contrato Aprobado';
  static const String message031 = 'Enviado para Revisión';
  static const String message032 = 'Enviado para Vo.Bo.';
  static const String message033 = 'Enviado a Participantes';
  static const String message034 = 'Enviado para Completar';
  static const String message035 = 'Pendiente de Firmar';
  static const String message036 = 'Enviado';
  static const String message037 = 'Ya firmaste, pendiente de validación.';
  static const String message038 = 'Enviado a Participantes';
  static const String message039 = 'PDF Cargado';
  static const String message040 = 'Finalizado';
  static const String message041 = 'Cancelado';
  static const String message042 = 'Negociado';
  static const String message043 = 'Vencido';
  static const String message044 = 'En progreso';
  static const String message045 = 'Preaprobado';
  static const String message046 = 'Aceptar términos y condiciones';
  static const String message047 = 'Contrato cancelado.';
  static const String message049 = 'Ya firmaste, en espera de validación.';

  static const String status001 = 'RECEIVED_TO_VOTE';
  static const String status002 = 'NEGOTIATION_TAKEN';
  static const String status003 = 'IS_YOUR_TURN_TO_SIGN_SELF';
  static const String status004 = 'IS_YOUR_TURN_TO_SIGN';
  static const String status005 = 'IS_NOT_YOUR_TURN_TO_SIGN_SELF';
  static const String status006 = 'IS_NOT_YOUR_TURN_TO_SIGN';
  static const String status007 = 'SENT_TO_VALIDATE';
  static const String status008 = 'due';
  static const String status009 = 'due_days';
  static const String status010 = 'EMPTY';
  static const String status011 = 'APPROVED';
  static const String status012 = 'RECEIVED';
  static const String status013 = 'RECEIVED_TO_FILL';
  static const String status014 = 'SENT_FILLED';
  static const String status015 = 'SENT_TO_FILL';
  static const String status016 = 'RECEIVED_FILLED';
  static const String status017 = 'RECEIVED_TO_SIGN';
  static const String status018 = 'RECEIVED_TO_ACCEPT';
  static const String status019 = 'RECEIVED_TO_REVIEW';
  static const String status020 = 'RECEIVED_TO_VOBO';
  static const String status021 = 'RECEIVED_SIGNED';
  static const String status022 = 'SENT_TO_ACCEPT';
  static const String status023 = 'REJECTED';
  static const String status024 = 'ACCEPTED';
  static const String status025 = 'AUTHORIZE';
  static const String status026 = 'PENDING_VALIDATION';
  static const String status027 = 'SENT_TO_DOWNLOAD';
  static const String status028 = 'NEGOTIATION';
  static const String status029 = 'PENDING_NEGOTIATION';
  static const String status030 = 'ACCEPTED_NEGOTIATION';
  static const String status031 = 'SENT_TO_REVIEW';
  static const String status032 = 'SENT_TO_VOBO';
  static const String status033 = 'RECEIVED_TO_DOWNLOAD';
  static const String status034 = 'FILLING_PENDING';
  static const String status035 = 'SENT_TO_SIGN';
  static const String status036 = 'SENT_WHICH';
  static const String status037 = 'SENT_SIGNED';
  static const String status038 = 'SEND_TO_DOWNLOAD';
  static const String status039 = 'LOADED';
  static const String status040 = 'FINALIZED';
  static const String status041 = 'CANCELED';
  static const String status042 = 'NEGOTIATED';
  static const String status043 = 'EXPIRED';
  static const String status044 = 'IN_PROGRESS';
  static const String status045 = 'PRE_APPROVED';
  static const String status047 = 'contractCanceled';
  static const String status048 = 'hasToAcceptPrivacyAgreement';
  static const String status049 = 'notInvolved';
  static const String status050 = 'notReadyToSign';

  static String alertPermissions =
      'Para realizar la grabación es necesario otorgue los permisos solicitados por la Aplicación de Trato.';

  static String alertPermissionsQR =
      'Para acceder al scanner es necesario otorgue los permisos solicitados por la Aplicación de Trato.';
  static String alertLocationPermissions =
      'Para trazar su firma es necesario otorgue los permisos solicitados por la Aplicación de Trato.';

  static String termsAndConditionsUrl = "https://trato.io/terms";
  static String privacyPolicyUrl = "https://trato.io/privacy";

  static String alertEmptySignatures =
      "Es necesario que firmes el documento trazando tu firma en el recuadro.";

  static const String armour4_no_available = "Blindaje 4 no disponible";

  static const String autograph = "autograph";

  static const String fingerprint_title = "Toma de huellas digitales";
  static const String fingerprint_content =
      "Haremos la toma de huellas digitales para Blindaje 4";

  static const String appChannelName = 'trato.io';
  static const String fingerIdPlugin = 'fingerid.plugin';

  static const String getFingerId = 'getFingerId';

  static const String contentB2Success =
      'Todos los participantes han firmado, ahora puedes descargar el documento.';

  static const String titleB2Success = 'El documento ha sido firmado';

  static const String title_uploading_fingerprint =
      'Estamos procesando tu huella digital';
  static const String content_uploading_fingerprint =
      'En unos isntantes podrás continuar.';
  static const String content_success_uploading_fingerprints =
      "Ahora debes esperar a que el dueño del contrato valide tus huellas digitales";
  static const String alertContractNoAvailable =
      "Este tipo de contrato pronto estará disponible para que lo puedas firmar desde Trato Sign,por ahora deberás realizar la firma en PC";

  static const String b1SuccessTitle = 'El documento esta listo para descargar';
  static const String b1SuccessContent =
      ' Descarga el documento, imprímelo y fírmalo de manera tradicional con los participantes.';

  static const String alertYouAreNotParticipant =
      'No puedes visualizar este documento porque no estas involucrado en el proceso de firma';
  static const String alertCanceledContract =
      'Este documento ha sido cancelado por el remitente.';
  static const String alertContractNotReady =
      'Este documento no está listo para ser firmado.';
}
