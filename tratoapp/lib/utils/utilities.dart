import 'package:tratoapp/utils/strings.dart';

class Utilities {
  static const String pathRegex = r'/sign/(\w+)/([A-Za-z0-9\-]+)?';

  String getUuid(String path) {
    RegExp regExp = new RegExp(pathRegex);
    if (regExp.hasMatch(path)) {
      var match = regExp.allMatches(path);
      return match.elementAt(0).group(2);
    } else {
      return "";
    }
  }

  String getContractId(String path) {
    RegExp regExp = new RegExp(pathRegex);
    if (regExp.hasMatch(path)) {
      var match =
          regExp.allMatches(path); // => extract the first (and only) match
      return match.elementAt(0).group(1);
    } else {
      return "";
    }
  }

  static String getStatus(String status) {
    String result = "Debe aceptar términos y condiciones";
    if (status == StringConstants.status001) {
      result = StringConstants.message001;
    } else if (status == StringConstants.status002) {
      result = StringConstants.message002;
    } else if (status == StringConstants.status003) {
      result = StringConstants.message003;
    } else if (status == StringConstants.status004) {
      result = StringConstants.message004;
    } else if (status == StringConstants.status005) {
      result = StringConstants.message005;
    } else if (status == StringConstants.status006) {
      result = StringConstants.message006;
    } else if (status == StringConstants.status007) {
      result = StringConstants.message007;
    } else if (status == StringConstants.status008) {
      result = StringConstants.message008;
    } else if (status == StringConstants.status009) {
      result = StringConstants.message009;
    } else if (status == StringConstants.status010) {
      result = StringConstants.message010;
    } else if (status == StringConstants.status011) {
      result = StringConstants.message011;
    } else if (status == StringConstants.status012) {
      result = StringConstants.message012;
    } else if (status == StringConstants.status013) {
      result = StringConstants.message013;
    } else if (status == StringConstants.status014) {
      result = StringConstants.message014;
    } else if (status == StringConstants.status015) {
      result = StringConstants.message015;
    } else if (status == StringConstants.status016) {
      result = StringConstants.message016;
    } else if (status == StringConstants.status017) {
      result = StringConstants.message017;
    } else if (status == StringConstants.status018) {
      result = StringConstants.message018;
    } else if (status == StringConstants.status019) {
      result = StringConstants.message019;
    } else if (status == StringConstants.status020) {
      result = StringConstants.message020;
    } else if (status == StringConstants.status021) {
      result = StringConstants.message021;
    } else if (status == StringConstants.status022) {
      result = StringConstants.message022;
    } else if (status == StringConstants.status023) {
      result = StringConstants.message023;
    } else if (status == StringConstants.status024) {
      result = StringConstants.message024;
    } else if (status == StringConstants.status025) {
      result = StringConstants.message025;
    } else if (status == StringConstants.status026) {
      result = StringConstants.message026;
    } else if (status == StringConstants.status027) {
      result = StringConstants.message027;
    } else if (status == StringConstants.status028) {
      result = StringConstants.message028;
    } else if (status == StringConstants.status029) {
      result = StringConstants.message029;
    } else if (status == StringConstants.status030) {
      result = StringConstants.message030;
    } else if (status == StringConstants.status031) {
      result = StringConstants.message031;
    } else if (status == StringConstants.status032) {
      result = StringConstants.message032;
    } else if (status == StringConstants.status033) {
      result = StringConstants.message033;
    } else if (status == StringConstants.status034) {
      result = StringConstants.message034;
    } else if (status == StringConstants.status035) {
      result = StringConstants.message035;
    } else if (status == StringConstants.status036) {
      result = StringConstants.message036;
    } else if (status == StringConstants.status037) {
      result = StringConstants.message037;
    } else if (status == StringConstants.status038) {
      result = StringConstants.message038;
    } else if (status == StringConstants.status039) {
      result = StringConstants.message039;
    } else if (status == StringConstants.status040) {
      result = StringConstants.message040;
    } else if (status == StringConstants.status041) {
      result = StringConstants.message041;
    } else if (status == StringConstants.status042) {
      result = StringConstants.message042;
    } else if (status == StringConstants.status043) {
      result = StringConstants.message043;
    } else if (status == StringConstants.status044) {
      result = StringConstants.message044;
    } else if (status == StringConstants.status045) {
      result = StringConstants.message045;
    } else if (status == StringConstants.status047) {
      result = StringConstants.message047;}
    else if (status == StringConstants.status048) {
      result = StringConstants.message046;
    } else {
      result = StringConstants.message045;
    }
    return result;
  }
}
