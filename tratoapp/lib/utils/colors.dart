import 'dart:ui';

class TratoColors {
  TratoColors._();
  static const Color orange = Color.fromRGBO(244, 122, 51, 1.0);
  static const Color greyishBrown = Color.fromRGBO(74, 74, 74, 1.0);
  static const Color gray = Color.fromRGBO(207, 207, 207, 1.0);
}
