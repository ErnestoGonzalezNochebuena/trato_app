import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:tratoapp/constants/api_urls.dart';

class DynamicLinkService {
  Future handleDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLinkData) async {
      _handleDeepLink(dynamicLinkData);
    }, onError: (OnLinkErrorException linkErrorException) async {
      print('Dynamic Link Failed: ${linkErrorException.message}');
    });

    this._handleDeepLink(data);
  }

  void _handleDeepLink(PendingDynamicLinkData data) {
    final Uri deepLink = data?.link;
    if (deepLink != null && Urls.devEnv()) {
      print('_handleDeepLink | deepLink: $deepLink');
    }
  }
}
