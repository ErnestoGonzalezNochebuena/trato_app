import 'package:tratoapp/constants/api_urls.dart';

class ApplicationSettings {
  static String pusherApiKey =
      !Urls.devEnv() ? '2c9bb078de6c4bc5a501' : '0587a01b6c06efdd3eb6';
  static String pusherId = !Urls.devEnv() ? '619343' : '869151';
  static String pusherSecretKey =
      !Urls.devEnv() ? '73f385e260b52b704b02' : 'f83bdc8c355f28e51a95';
  static String pusherCluster = !Urls.devEnv() ? 'mt1' : 'mt1';
  static const String pusherChannelPrefix = 'private-';
  static const String pusherEventName = 'client-refresh-all';
  static String pusherEndpoint = "https://${Urls.endpoint()}/auth";
  static const String pusherConnected = "connected";
  static const String receiver = "Receptor";
}
