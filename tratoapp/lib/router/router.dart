import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:tratoapp/ui/about_us/about_us_screen.dart';
import 'package:tratoapp/ui/contracts/contract_list_screen.dart';
import 'package:tratoapp/ui/contracts/contract_screen.dart';
import 'package:tratoapp/ui/fingerprint/fingerprint_screen.dart';
import 'package:tratoapp/ui/fingerprint/upload_fingerprint_screen.dart';
import 'package:tratoapp/ui/signature/signature_screen.dart';
import 'package:tratoapp/ui/signature/success_signature_screen.dart';
import 'package:tratoapp/ui/splash/splash_screen.dart';
import 'package:tratoapp/ui/terms_conditions/accept_terms_screen.dart';
import 'package:tratoapp/ui/terms_conditions/policies_screen.dart';
import 'package:tratoapp/ui/validation/success_validation_screen.dart';
import 'package:tratoapp/ui/validation/waiting_validation_screen.dart';
import 'package:tratoapp/ui/video_capture/camera_overlay_screen.dart';
import 'package:tratoapp/ui/video_capture/upload_video_screen.dart';
import 'package:tratoapp/ui/video_capture/video_capture_screen.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  SplashScreen splashScreen;
  AboutUsScreen aboutUsScreen;
  FingerprintScreen fingerprintScreen;
  VideoCaptureScreen videoCaptureScreen;
  CameraOverlayScreen cameraOverlayScreen;
  ContractScreen contractScreen;
  UploadVideoScreen uploadVideoScreen;
  UploadFingerprintScreen uploadFingerprintScreen;
  WaitingValidationScreen waitingValidationScreen;
  SuccessValidationScreen successValidationScreen;
  SignatureScreen signatureScreen;
  SuccessSignatureScreen successSignatureScreen;
  ContractListScreen contractListScreen;
  AcceptConditions acceptConditions;
  PoliciesScreen policiesScreen;

}
