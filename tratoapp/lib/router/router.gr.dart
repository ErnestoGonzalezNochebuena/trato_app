// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:tratoapp/ui/splash/splash_screen.dart';
import 'package:tratoapp/ui/about_us/about_us_screen.dart';
import 'package:tratoapp/ui/fingerprint/fingerprint_screen.dart';
import 'package:tratoapp/model/contract_model.dart';
import 'package:tratoapp/ui/video_capture/video_capture_screen.dart';
import 'package:tratoapp/ui/video_capture/camera_overlay_screen.dart';
import 'package:tratoapp/ui/contracts/contract_screen.dart';
import 'package:tratoapp/ui/video_capture/upload_video_screen.dart';
import 'package:tratoapp/ui/fingerprint/upload_fingerprint_screen.dart';
import 'package:tratoapp/ui/validation/waiting_validation_screen.dart';
import 'package:tratoapp/ui/validation/success_validation_screen.dart';
import 'package:tratoapp/ui/signature/signature_screen.dart';
import 'package:tratoapp/ui/signature/success_signature_screen.dart';
import 'package:tratoapp/ui/contracts/contract_list_screen.dart';
import 'package:tratoapp/ui/terms_conditions/accept_terms_screen.dart';
import 'package:tratoapp/ui/terms_conditions/policies_screen.dart';

abstract class Routes {
  static const splashScreen = '/';
  static const aboutUsScreen = '/about-us-screen';
  static const fingerprintScreen = '/fingerprint-screen';
  static const videoCaptureScreen = '/video-capture-screen';
  static const cameraOverlayScreen = '/camera-overlay-screen';
  static const contractScreen = '/contract-screen';
  static const uploadVideoScreen = '/upload-video-screen';
  static const uploadFingerprintScreen = '/upload-fingerprint-screen';
  static const waitingValidationScreen = '/waiting-validation-screen';
  static const successValidationScreen = '/success-validation-screen';
  static const signatureScreen = '/signature-screen';
  static const successSignatureScreen = '/success-signature-screen';
  static const contractListScreen = '/contract-list-screen';
  static const acceptConditions = '/accept-conditions';
  static const policiesScreen = '/policies-screen';
  static const all = {
    splashScreen,
    aboutUsScreen,
    fingerprintScreen,
    videoCaptureScreen,
    cameraOverlayScreen,
    contractScreen,
    uploadVideoScreen,
    uploadFingerprintScreen,
    waitingValidationScreen,
    successValidationScreen,
    signatureScreen,
    successSignatureScreen,
    contractListScreen,
    acceptConditions,
    policiesScreen,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.splashScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SplashScreen(),
          settings: settings,
        );
      case Routes.aboutUsScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => AboutUsScreen(),
          settings: settings,
        );
      case Routes.fingerprintScreen:
        if (hasInvalidArgs<FingerprintScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<FingerprintScreenArguments>(args);
        }
        final typedArgs = args as FingerprintScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              FingerprintScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.videoCaptureScreen:
        if (hasInvalidArgs<VideoCaptureScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<VideoCaptureScreenArguments>(args);
        }
        final typedArgs = args as VideoCaptureScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              VideoCaptureScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.cameraOverlayScreen:
        if (hasInvalidArgs<CameraOverlayScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<CameraOverlayScreenArguments>(args);
        }
        final typedArgs = args as CameraOverlayScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              CameraOverlayScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.contractScreen:
        if (hasInvalidArgs<ContractScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<ContractScreenArguments>(args);
        }
        final typedArgs = args as ContractScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => ContractScreen(
              isSignPreview: typedArgs.isSignPreview,
              contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.uploadVideoScreen:
        if (hasInvalidArgs<UploadVideoScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<UploadVideoScreenArguments>(args);
        }
        final typedArgs = args as UploadVideoScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => UploadVideoScreen(
              videoPath: typedArgs.videoPath,
              contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.uploadFingerprintScreen:
        if (hasInvalidArgs<UploadFingerprintScreenArguments>(args)) {
          return misTypedArgsRoute<UploadFingerprintScreenArguments>(args);
        }
        final typedArgs = args as UploadFingerprintScreenArguments ??
            UploadFingerprintScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => UploadFingerprintScreen(
              contractModel: typedArgs.contractModel,
              fingerprintResponse: typedArgs.fingerprintResponse),
          settings: settings,
        );
      case Routes.waitingValidationScreen:
        if (hasInvalidArgs<WaitingValidationScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<WaitingValidationScreenArguments>(args);
        }
        final typedArgs = args as WaitingValidationScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              WaitingValidationScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.successValidationScreen:
        if (hasInvalidArgs<SuccessValidationScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<SuccessValidationScreenArguments>(args);
        }
        final typedArgs = args as SuccessValidationScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              SuccessValidationScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.signatureScreen:
        if (hasInvalidArgs<SignatureScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<SignatureScreenArguments>(args);
        }
        final typedArgs = args as SignatureScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              SignatureScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.successSignatureScreen:
        if (hasInvalidArgs<SuccessSignatureScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<SuccessSignatureScreenArguments>(args);
        }
        final typedArgs = args as SuccessSignatureScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              SuccessSignatureScreen(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.contractListScreen:
        if (hasInvalidArgs<ContractListScreenArguments>(args)) {
          return misTypedArgsRoute<ContractListScreenArguments>(args);
        }
        final typedArgs = args as ContractListScreenArguments ??
            ContractListScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => ContractListScreen(
              isBackFromContract: typedArgs.isBackFromContract),
          settings: settings,
        );
      case Routes.acceptConditions:
        if (hasInvalidArgs<AcceptConditionsArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<AcceptConditionsArguments>(args);
        }
        final typedArgs = args as AcceptConditionsArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              AcceptConditions(contractModel: typedArgs.contractModel),
          settings: settings,
        );
      case Routes.policiesScreen:
        if (hasInvalidArgs<PoliciesScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<PoliciesScreenArguments>(args);
        }
        final typedArgs = args as PoliciesScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              PoliciesScreen(informationType: typedArgs.informationType),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//FingerprintScreen arguments holder class
class FingerprintScreenArguments {
  final ContractModel contractModel;
  FingerprintScreenArguments({@required this.contractModel});
}

//VideoCaptureScreen arguments holder class
class VideoCaptureScreenArguments {
  final ContractModel contractModel;
  VideoCaptureScreenArguments({@required this.contractModel});
}

//CameraOverlayScreen arguments holder class
class CameraOverlayScreenArguments {
  final ContractModel contractModel;
  CameraOverlayScreenArguments({@required this.contractModel});
}

//ContractScreen arguments holder class
class ContractScreenArguments {
  final bool isSignPreview;
  final ContractModel contractModel;
  ContractScreenArguments(
      {@required this.isSignPreview, @required this.contractModel});
}

//UploadVideoScreen arguments holder class
class UploadVideoScreenArguments {
  final String videoPath;
  final ContractModel contractModel;
  UploadVideoScreenArguments(
      {@required this.videoPath, @required this.contractModel});
}

//UploadFingerprintScreen arguments holder class
class UploadFingerprintScreenArguments {
  final ContractModel contractModel;
  final String fingerprintResponse;
  UploadFingerprintScreenArguments(
      {this.contractModel, this.fingerprintResponse});
}

//WaitingValidationScreen arguments holder class
class WaitingValidationScreenArguments {
  final ContractModel contractModel;
  WaitingValidationScreenArguments({@required this.contractModel});
}

//SuccessValidationScreen arguments holder class
class SuccessValidationScreenArguments {
  final ContractModel contractModel;
  SuccessValidationScreenArguments({@required this.contractModel});
}

//SignatureScreen arguments holder class
class SignatureScreenArguments {
  final ContractModel contractModel;
  SignatureScreenArguments({@required this.contractModel});
}

//SuccessSignatureScreen arguments holder class
class SuccessSignatureScreenArguments {
  final ContractModel contractModel;
  SuccessSignatureScreenArguments({@required this.contractModel});
}

//ContractListScreen arguments holder class
class ContractListScreenArguments {
  final bool isBackFromContract;
  ContractListScreenArguments({this.isBackFromContract = false});
}

//AcceptConditions arguments holder class
class AcceptConditionsArguments {
  final ContractModel contractModel;
  AcceptConditionsArguments({@required this.contractModel});
}

//PoliciesScreen arguments holder class
class PoliciesScreenArguments {
  final int informationType;
  PoliciesScreenArguments({@required this.informationType});
}
